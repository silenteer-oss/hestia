package app

import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/google/uuid"
	"gitlab.com/silenteer-oss/hestia/socket_service/internal/app/socket"
	"gitlab.com/silenteer-oss/hestia/socket_service/internal/app/socket/manager"
	"gitlab.com/silenteer-oss/hestia/socket_service/internal/app/socket/model"
	"log"
	"net/http"
	"os"
	"os/signal"
	"strings"
	"syscall"
	"time"

	"github.com/gorilla/websocket"
	"gitlab.com/silenteer-oss/hestia/socket_service/api"
	"gitlab.com/silenteer-oss/titan"
	"logur.dev/logur"
)

type sessionInfo struct {
	SessionId string `json:"sessionId"`
	UserId    string `json:"userId"`
}

//`Credentials Do Not Match`

type socketGatewayServer struct {
	logger       logur.Logger
	httpServer   *http.Server
	authService  *AuthService
	httpConfig   *HttpConfig
	natServer    *titan.Server
	socketManger *manager.SocketManager
	socketClient *api.SocketServiceClient
}

func InitSocketGateWay(socketManager *manager.SocketManager, natServer *titan.Server, logger logur.Logger, authService *AuthService, httpConfig *HttpConfig) *socketGatewayServer {
	return &socketGatewayServer{
		socketManger: socketManager,
		natServer:    natServer,
		logger:       logger,
		authService:  authService,
		httpConfig:   httpConfig,
		socketClient: api.NewSocketServiceClient(),
	}
}

func (srv *socketGatewayServer) Start() {
	// Start socket Gateway Server
	srv.socketManger.Start()

	go func() {
		srv.natServer.Start()
	}()

	httpS := &http.Server{
		Addr:    fmt.Sprintf(":%d", srv.httpConfig.SocketPort),
		Handler: srv,
	}

	srv.httpServer = httpS

	done := make(chan os.Signal, 1)
	signal.Notify(done, os.Interrupt, syscall.SIGINT, syscall.SIGTERM)

	go func() {
		if err := httpS.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			srv.logger.Error(fmt.Sprintf("listen: %s\n", err))
			os.Exit(1)
		}
	}()
	srv.logger.Info(fmt.Sprintf("Http Server Started at %d", srv.httpConfig.SocketPort))

	<-done
	srv.logger.Info("Http Server Stopped")

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer func() {
		cancel()
		srv.natServer.Stop()
		srv.httpServer.Close()
		srv.socketManger.CloseAll()
	}()

	if err := httpS.Shutdown(ctx); err != nil {
		srv.logger.Error(fmt.Sprintf("Http Server Shutdown Failed:%+v", err))
		os.Exit(1)
	}
	srv.logger.Info("Socket Server Exited Properly")
}

func (srv *socketGatewayServer) Stop() {
	if srv.httpServer != nil {
		_ = srv.httpServer.Shutdown(context.Background())
	}
}

func (srv *socketGatewayServer) upgradeToSocketConnection(w http.ResponseWriter, r *http.Request) (*websocket.Conn, error) {
	upgrader := websocket.Upgrader{}

	upgrader.CheckOrigin = func(r *http.Request) bool { return true }

	wsConn, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		srv.logger.Error(fmt.Sprintf("Upgrade socket connection error %+v", err))
		return nil, err
	}
	return wsConn, nil
}

func (srv *socketGatewayServer) handleCommonRequest(w http.ResponseWriter, r *http.Request) {
	// check and set header
	if r.Header.Get(titan.XRequestId) == "" {
		r.Header.Set(titan.XRequestId, titan.RandomString(6))
	}

	r.Header.Set(titan.XOrigin, strings.Split(r.Host, ":")[0])

	//srv.logger.Info("socketGateway: http request received:", map[string]interface{}{"url": r.RequestURI, "id": r.Header.Get("X-REQUEST-ID"), "method": r.Method})
	userInfo, err := srv.authService.validateToken(r)
	if err != nil {
		srv.logger.Error(fmt.Sprintf("Request authentication error %+v", err))
		w.WriteHeader(500)
		return
	}

	if userInfo == "" {
		srv.logger.Error(fmt.Sprintf("Unauthorized"))
		w.WriteHeader(401)
		return
	}

	//Parse user to Session
	session := &model.Session{}
	err = json.Unmarshal([]byte(userInfo), session)
	if err != nil {
		srv.logger.Error(fmt.Sprintf("Session Unmarshal Error %+v", err))
		w.WriteHeader(400)
		return
	}

	wsConn, err := srv.upgradeToSocketConnection(w, r)
	if err != nil {
		srv.logger.Error(fmt.Sprintf("Upgrade Socket connection error %+v", err))
		w.WriteHeader(500)
		return
	}
	client := socket.CreateCommonSocket(session, wsConn, srv.socketManger, srv.logger)

	var userID string
	if len(session.ExternalUserId) > 0 {
		userID = session.ExternalUserId.String()
	} else {
		userID = session.UserId.String()
	}

	openMessage := srv.makeOpenMessage(client.GetId(), userID)
	wsConn.WriteJSON(openMessage)

	srv.logger.Debug(fmt.Sprintf("%s %s", userID, client.GetId()))

	err = srv.socketClient.NotifySocketConnCreated(titan.NewBackgroundContext(),
		&api.NotifySocketConnCreatedRequest{
			SessionId: client.GetId(),
			UserId:    userID,
		})

	if err != nil {
		srv.logger.Error(fmt.Sprintf("err publish new conn %v", err))
	}

	client.StartHandler()
}

func (srv *socketGatewayServer) handleCompanionsRequest(w http.ResponseWriter, r *http.Request) {
	if r.Header.Get(titan.XRequestId) == "" {
		r.Header.Set(titan.XRequestId, titan.RandomString(6))
	}
	//TODO: Check JWT, companionId

	wsConn, err := srv.upgradeToSocketConnection(w, r)
	if err != nil {
		srv.logger.Error(fmt.Sprintf("Upgrade Socket connection error %+v", err))
		w.WriteHeader(500)
		return
	}
	session := model.Session{
		ExternalUserId:  "",
		UserId:          "",
		CareProviderId:  titan.UUID(uuid.New().String()),
		CareProviderKey: "",
		DeviceId:        model.TestDeviceId.String(), //Todo: gen info from auth
		Role:            "companion",
		Attributes:      nil,
	}

	client := socket.NewRequestResponseSocket(&session, wsConn, srv.socketManger, srv.logger)
	openMessage, err := socket.BuildOpenMessage(client.GetId(), session.DeviceId)
	if err != nil {
		srv.logger.Error(fmt.Sprintf("Open ws has error %+v", err))
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	wsConn.WriteJSON(openMessage)
	client.StartHandler()

}

func (srv *socketGatewayServer) handleTrustedDeviceRequest(w http.ResponseWriter, r *http.Request) {

	// check and set header
	if r.Header.Get(titan.XRequestId) == "" {
		r.Header.Set(titan.XRequestId, titan.RandomString(6))
	}
	var deviceId string

	n, err := fmt.Sscanf(r.RequestURI, "/ws/device/%s", &deviceId)
	if err != nil {
		srv.logger.Error(fmt.Sprintf("Read socket URL error %+v", err))
		w.WriteHeader(500)
		return
	}
	if n <= 0 {
		srv.logger.Error(fmt.Sprintf("wrong socket URL request"))
		w.WriteHeader(400)
		return
	}

	log.Println("socketGateway: trusted device request received:", map[string]interface{}{"url": r.RequestURI, "id": r.Header.Get("X-REQUEST-ID"), "method": r.Method})

	wsConn, err := srv.upgradeToSocketConnection(w, r)
	if err != nil {
		w.WriteHeader(500)
		return
	}
	session := &model.Session{
		DeviceId: deviceId,
	}

	client := socket.CreatePairingSocket(session, wsConn, srv.socketManger, srv.logger)
	client.StartHandler()
}

func (srv *socketGatewayServer) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	defer func() {
		if r := recover(); r != nil {
			var ok bool
			var err error
			err, ok = r.(error)
			if !ok {
				err = fmt.Errorf("panic : %v", r)
			}
			srv.logger.Error(fmt.Sprintf("&&&&  HTTP Sockket panic error %+v\n ", err))
		}
	}()

	srv.logger.Info("Client is opening socket connection ________________________________________")
	// route request base on url
	if strings.HasPrefix(r.RequestURI, "/ws/common") {
		srv.handleCommonRequest(w, r)
	}

	if strings.HasPrefix(r.RequestURI, "/ws/device") {
		srv.handleTrustedDeviceRequest(w, r)
	}

	//TODO: will enable this url when companion/socket.go is done.
	if strings.HasPrefix(r.RequestURI, "/ws/companion") {
		srv.handleCompanionsRequest(w, r)
	}
}

func (srv *socketGatewayServer) makeOpenMessage(sessionId string, userId string) map[string]interface{} {
	sessionInfo := &sessionInfo{
		SessionId: sessionId,
		UserId:    userId,
	}

	v, err := json.Marshal(sessionInfo)
	if err != nil {
		srv.logger.Error(fmt.Sprintf("Marshal sessionInfo error %+v\n ", err))
	}
	openMessage := map[string]interface{}{
		"messageBody": string(v),
		"topic":       "OPEN_COMMON_WS",
	}

	return openMessage
}
