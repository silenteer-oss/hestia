package manager

import (
	"encoding/json"
	"fmt"
	"gitlab.com/silenteer-oss/hestia/socket_service/internal/app/socket/model"
	. "gitlab.com/silenteer-oss/hestia/socket_service/internal/app/socket/socket_type"
	"strconv"

	"gitlab.com/silenteer-oss/titan"

	"gitlab.com/silenteer-oss/hestia/socket_service/api"

	"time"

	"emperror.dev/errors"
	"logur.dev/logur"
)

// --------------------------- Socket manager code

type BroadcastInfo struct {
	message []byte
	filter  func(session *model.Session) bool
}

// Hub maintains the set of active clients and broadcasts messages to the
// clients.
type SocketManager struct {
	clients map[string]Socket // session id => client

	// Inbound messages from the clients.
	broadcast chan *BroadcastInfo

	// Register requests from the clients.
	register chan Socket

	// Unregister requests from clients.
	unregister chan Socket

	logger logur.Logger
}

func InitSocketManager(logger logur.Logger) *SocketManager {
	return &SocketManager{
		broadcast:  make(chan *BroadcastInfo, 1000),
		register:   make(chan Socket, 1000),
		unregister: make(chan Socket, 1000),
		clients:    make(map[string]Socket),
		logger:     logger,
	}
}

func (m *SocketManager) Register(client Socket) {
	select {
	case m.register <- client:
		m.logger.Debug(fmt.Sprintf("Register client %s", client.GetId()))
	default:
		m.logger.Error("Cannot write to channel 'register'. Please increase its buffer size")
	}
}

func (m *SocketManager) UnRegister(client Socket) {
	select {
	case m.unregister <- client:
	default:
		m.logger.Error("Cannot write to channel 'unregister'. Please increase its buffer size")
	}

}

func (m *SocketManager) Broadcast(message []byte, filter func(session *model.Session) bool) {
	m.broadcast <- &BroadcastInfo{
		message: message,
		filter:  filter,
	}
}

func (m *SocketManager) GetClient(filter func(session *model.Session) bool) (client Socket, found bool) {
	for _, client := range m.clients {
		if filter(client.GetSession()) {
			return client, true
		}
	}
	return nil, false
}

func (m *SocketManager) BroadcastM(message *api.SendRequestMessage, filter func(session *model.Session) bool) error {
	messageByte, err := json.Marshal(message)
	if err != nil {
		return errors.WithMessage(err, "Marshal message error")
	}
	m.Broadcast(messageByte, filter)
	return nil
}

func (m *SocketManager) run() {
	ticker := time.NewTicker(30 * time.Second)
	defer func() {
		ticker.Stop()
	}()
	for {
		select {
		case client := <-m.register:
			m.clients[client.GetId()] = client
			m.logger.Debug("Register client ", map[string]interface{}{"id": client.GetId(), "Total remain": len(m.clients)})

		case client := <-m.unregister:
			if _, ok := m.clients[client.GetId()]; ok {
				delete(m.clients, client.GetId())
				client.Close()
			}
			m.logger.Debug("Unregister client ", map[string]interface{}{"id": client.GetId(), "Total remain": len(m.clients)})
		case info := <-m.broadcast:
			for _, client := range m.clients {
				if info.filter(client.GetSession()) {
					client.Send(info.message)
				}
			}
		case <-ticker.C:
			m.logger.Debug("++++++++++++++++++ socket manager is running ....., total= " + strconv.Itoa(len(m.clients)))
		}
	}
}

func (m *SocketManager) Start() {
	go panicRecover(m.run)
}

func (m *SocketManager) CloseAll() {
	for _, socketClient := range m.clients {
		socketClient.Close()
	}
}

func (m *SocketManager) Close(socket Socket) {
	err := socket.SendCloseControl()
	if err != nil {
		m.logger.Debug("Close connection error")
	}
}

func (m *SocketManager) CloseByUserId(userId titan.UUID, sessionId string) {
	for _, client := range m.clients {
		session := client.GetSession()
		if session == nil || client.GetId() == sessionId {
			continue
		}

		if (len(session.ExternalUserId) > 0 && session.ExternalUserId == userId) ||
			(len(session.UserId) > 0 && session.UserId == userId) {
			m.logger.Info(fmt.Sprintf("^^^^^ Close %s %s", userId, client.GetId()))
			m.Close(client)
		}
	}
}
