package socket

import (
	"encoding/json"
	"github.com/pkg/errors"
	"gitlab.com/silenteer-oss/hestia/infrastructure"
	"gitlab.com/silenteer-oss/hestia/socket_service/api"
	"gitlab.com/silenteer-oss/hestia/socket_service/internal/app/socket/manager"
	"gitlab.com/silenteer-oss/hestia/socket_service/internal/app/socket/model"
	"gitlab.com/silenteer-oss/hestia/socket_service/internal/app/socket/socket_type"
	"gitlab.com/silenteer-oss/titan"
	"gitlab.com/silenteer-oss/titan/flag"
)

type Service struct {
	socketManager              *manager.SocketManager
	duplicateUserSocketAllowed bool
}

func (s *Service) SendToDeviceWithResponse(ctx *titan.Context, request *api.SendToDeviceWithResponseRequest) (*api.SendToDeviceResponse, error) {
	if request.DeviceId == nil {
		return nil, errors.New("companion id is required")
	}
	var response = &api.SendToDeviceResponse{
		DeviceId: &model.TestDeviceId,
	}

	sClient, found := s.socketManager.GetClient(func(session *model.Session) bool {
		return session.DeviceId == request.DeviceId.String()
	})
	var responsibleSocket socket_type.SocketRequestResponse
	if found {
		responsibleSocket, found = sClient.(socket_type.SocketRequestResponse)
	}
	// when this instance of socket not have connection with companion. Need to set skip response for letting other instance process.
	if !found {
		ctx.SetResponseHeader(flag.HeaderTerminatedFlag, flag.HeaderTerminatedValue)
		return nil, nil
	}

	titanRequest := titan.Request{
		Headers: ctx.Request().Headers,
		Body:    []byte(request.Body),
		URL:     request.Url,
	}

	dataResponse := responsibleSocket.Request(titanRequest)
	if len(dataResponse.Body) > 0 {
		v, err := json.Marshal(dataResponse.Body)
		if err != nil {
			return nil, errors.WithStack(err)
		}

		response = &api.SendToDeviceResponse{
			DeviceId: request.DeviceId,
			JsonData: string(v),
		}
	}
	return response, nil
}

func (s *Service) SendEventToDevice(ctx *titan.Context, request *api.SendEventToDeviceRequest) (*api.MessageResponse, error) {
	deviceId := request.DeviceId
	messageByte, err := json.Marshal(request.DeviceEvent)
	if err != nil {
		return nil, errors.WithMessage(err, "Marshal message error")
	}
	s.socketManager.Broadcast(messageByte, func(session *model.Session) bool {
		return session.DeviceId == deviceId
	})

	return &api.MessageResponse{}, nil
}

func (s *Service) SendToDevice(ctx *titan.Context, request *api.SendToDeviceRequest) (*api.MessageResponse, error) {
	deviceId := request.DeviceId

	err := s.socketManager.BroadcastM(request.MessageInfo, func(session *model.Session) bool {
		return session.DeviceId == deviceId
	})

	if err != nil {
		return nil, errors.WithMessage(err, "SendToDevice error")
	}

	return &api.MessageResponse{}, nil
}

func (s *Service) SendToUser(ctx *titan.Context, request *api.SendToUserRequest) (*api.MessageResponse, error) {
	userId := request.UserId
	userUUID := titan.UUID(userId.String())
	err := s.socketManager.BroadcastM(request.MessageInfo, func(session *model.Session) bool {
		return session.UserId == userUUID
	})
	return nil, err
}

func (s *Service) SendToUsers(ctx *titan.Context, request *api.SendToUsersRequest) (*api.SendToUsersResponse, error) {
	userIDs := make([]titan.UUID, 0)
	for _, id := range request.UserIds {
		userIDs = append(userIDs, titan.UUID(id.String()))
	}
	err := s.socketManager.BroadcastM(request.MessageInfo, func(session *model.Session) bool {
		for _, userId := range userIDs {
			if session.UserId == userId {
				return true
			}
		}
		return false
	})
	return &api.SendToUsersResponse{}, err
}

func (s *Service) SendToPatient(ctx *titan.Context, request *api.SendToPatientRequest) (*api.MessageResponse, error) {
	externalUserId := titan.UUID(request.ExternalUserId)
	err := s.socketManager.BroadcastM(request.MessageInfo, func(session *model.Session) bool {
		return session.ExternalUserId == externalUserId
	})
	return &api.MessageResponse{}, err
}

func (s *Service) SendToCareProviderPatients(ctx *titan.Context, request *api.SendToCareProviderPatientsRequest) (*api.MessageResponse, error) {
	careProviderId := titan.UUID(request.CareProviderId.String())
	err := s.socketManager.BroadcastM(request.MessageInfo, func(session *model.Session) bool {
		return session.CareProviderId == careProviderId && session.Role == infrastructure.PATIENT
	})
	return &api.MessageResponse{}, err
}

func (s *Service) SendToCareProviderMembers(ctx *titan.Context, request *api.SendToCareProviderMembersRequest) (*api.MessageResponse, error) {
	careProviderId := titan.UUID(request.CareProviderId.String())
	err := s.socketManager.BroadcastM(request.MessageInfo, func(session *model.Session) bool {
		return session.CareProviderId == careProviderId && session.Role == infrastructure.CARE_PROVIDER_MEMBER
	})
	return &api.MessageResponse{}, err
}

func (s *Service) NotifySocketConnCreated(ctx *titan.Context, request *api.NotifySocketConnCreatedRequest) error {
	if s.duplicateUserSocketAllowed {
		return nil
	}

	s.socketManager.CloseByUserId(titan.UUID(request.UserId), request.SessionId)
	return nil
}
func InitSocketService(skManager *manager.SocketManager, duplicateUserSocketAllowed bool) api.SocketService {
	return &Service{
		socketManager:              skManager,
		duplicateUserSocketAllowed: duplicateUserSocketAllowed,
	}
}
