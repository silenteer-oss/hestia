package socket

import (
	"github.com/gorilla/websocket"
	"gitlab.com/silenteer-oss/hestia/socket_service/internal/app/socket/manager"
	"gitlab.com/silenteer-oss/hestia/socket_service/internal/app/socket/model"
	"gitlab.com/silenteer-oss/titan"
	"logur.dev/logur"
)

// for pairing socket ----------------------------------------------------------------------------------------------
type PairingSocket struct {
	BaseSocket
}

func CreatePairingSocket(session *model.Session, conn *websocket.Conn, socketManager *manager.SocketManager, logger logur.Logger) *PairingSocket {
	id := titan.RandomString(20)
	socket := &PairingSocket{
		BaseSocket: BaseSocket{
			session:       session,
			conn:          conn,
			socketManager: socketManager,
			send:          make(chan []byte, 100),
			id:            titan.RandomString(20),
			logger:        logur.WithFields(logger, map[string]interface{}{"sessionId": id}),
		},
	}
	socket.onMessage = socket.OnMessage
	socketManager.Register(socket)
	return socket
}

func (t *PairingSocket) OnMessage(message []byte) {
	t.logger.Debug("Pairing Socket on message arrive ")
	t.socketManager.Broadcast(message, func(session *model.Session) bool {
		return t.session.DeviceId == session.DeviceId
	})
}
