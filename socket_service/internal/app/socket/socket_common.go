package socket

import (
	"encoding/json"
	"fmt"
	"gitlab.com/silenteer-oss/hestia/socket_service/internal/app/socket/manager"
	"gitlab.com/silenteer-oss/hestia/socket_service/internal/app/socket/model"
	"net/http"
	"time"

	"github.com/gorilla/websocket"
	"gitlab.com/silenteer-oss/hestia/socket_service/api"
	"gitlab.com/silenteer-oss/titan"
	"logur.dev/logur"
)

// ----------------------- Login user socket -----------------------------------------------------------------
type CommonSocket struct {
	BaseSocket
	natClient *titan.Client
}

var pong []byte

func init() {
	pongMessage := api.PONG
	code := int64(200)
	pong, _ = json.Marshal(api.MessageResponse{Topic: &pongMessage, StatusCode: &code})
}

func CreateCommonSocket(session *model.Session, conn *websocket.Conn, socketManager *manager.SocketManager, logger logur.Logger) *CommonSocket {
	id := titan.RandomString(20)
	socket := &CommonSocket{
		BaseSocket: BaseSocket{
			session:       session,
			conn:          conn,
			socketManager: socketManager,
			send:          make(chan []byte, 9000),
			id:            id,
			logger:        logur.WithFields(logger, map[string]interface{}{"sessionId": id}),
		},
		natClient: titan.GetDefaultClient(),
	}
	socket.onMessage = socket.OnMessage
	socketManager.Register(socket)

	return socket
}

func (c *CommonSocket) handleOnMessage(message []byte) {
	t := time.Now()
	defer func() {
		if r := recover(); r != nil {
			c.logger.Debug("Panic Recovered in socket handleOnMessage")
		}
	}()

	var request api.SendRequestMessage

	if err := json.Unmarshal(message, &request); err != nil {
		c.logger.Error(fmt.Sprintf("Unmashal common socket error %+v\n , id=%s", err, request.Id))
		c.sendValidationErrorResponse(fmt.Sprintf("Unmashal error %+v\n ", err), 400, request.ResponseTopic, request.Id, t)
		return
	}

	c.logger.Info(fmt.Sprintf("Socket server received message topic=%s,  id=%s", request.Topic, request.Id))

	if request.Topic == api.PING {
		c.Send(pong)
		c.logger.Debug(fmt.Sprintf("Socket server PONG id=%s", request.Id))
		return
	}

	if request.Subject == "" {
		c.sendValidationErrorResponse("Message NATS Subject cannot be null", 400, request.ResponseTopic, request.Id, t)
		return
	}

	if request.Topic == "" {
		c.sendValidationErrorResponse("Message Topic cannot be null", 400, request.ResponseTopic, request.Id, t)
		return
	}

	userInfo, err := c.GetSessionAsJsonString()
	if err != nil {
		c.sendValidationErrorResponse(
			fmt.Sprintf("Mashal response message  error %+v\n ", err),
			500,
			request.ResponseTopic,
			request.Id, t,
		)
		return
	}

	// forward request to Nats
	nRequest := &titan.Request{
		Method:  "POST",
		Headers: http.Header{},
		Subject: request.Subject,
		URL:     string(request.Topic),
	}

	if request.MessageBody != "" && len(request.MessageBody) > 0 {
		nRequest.Body = []byte(request.MessageBody)
	}

	nRequest.Headers.Set(titan.XUserInfo, userInfo)

	offsets := request.Headers[titan.XRequestTimeOffset]
	if len(offsets) > 0 {
		nRequest.Headers.Set(titan.XRequestTimeOffset, offsets[0])
	}

	if request.Id == "" {
		nRequest.Headers.Set(titan.XRequestId, titan.RandomString(6))
	} else {
		nRequest.Headers.Set(titan.XRequestId, request.Id)
	}

	resp, sendErr := c.natClient.SendRequest(titan.NewBackgroundContext(), nRequest)

	if sendErr != nil {
		if clientErr, ok := sendErr.(*titan.ClientResponseError); ok {
			resp = clientErr.Response
			if resp.Body == nil && len(resp.Body) <= 0 {
				body, _ := json.Marshal(titan.DefaultJsonError{
					Message: resp.Status,
					Links:   map[string][]string{"self": {}},
					TraceId: request.Id,
				})
				resp.Body = body
			}
		} else {
			c.sendValidationErrorResponse(
				fmt.Sprintf("NATS forwarding message  error %+v\n ", err),
				500,
				request.ResponseTopic,
				request.Id, t,
			)
			return
		}
	}

	if resp == nil || request.ResponseTopic == "" {
		return
	}
	statusCode := int64(resp.StatusCode)
	messageResponse := &api.MessageResponse{
		Topic:      &request.ResponseTopic,
		Headers:    resp.Headers,
		StatusCode: &statusCode,
		Id:         &request.Id,
	}
	if resp.Body != nil && len(resp.Body) > 2 {
		body := string(resp.Body)
		messageResponse.MessageBody = &body
	}
	c.sendMessageResponse(messageResponse, t)
}

func (c *CommonSocket) OnMessage(message []byte) {
	go c.handleOnMessage(message)
}

func (c *CommonSocket) sendValidationErrorResponse(message string, statusCode int, responseTopic api.Topic, id string, startTime time.Time) {
	if responseTopic == "" {
		return
	}
	body, _ := json.Marshal(titan.DefaultJsonError{
		Message: message,
		Links:   map[string][]string{"self": {}},
		TraceId: "",
	})
	bodyString := string(body)
	statusCode64 := int64(statusCode)

	messageResponse := &api.MessageResponse{
		Topic:       &responseTopic,
		MessageBody: &bodyString, // convert message to json object
		StatusCode:  &statusCode64,
		Id:          &id,
	}
	c.sendMessageResponse(messageResponse, startTime)
}

func (c *CommonSocket) sendMessageResponse(message *api.MessageResponse, startTime time.Time) {
	responseByte, err := json.Marshal(message)
	if err != nil {
		c.logger.Error(fmt.Sprintf("Mashal response message  error %+v\n, id= %v", err, message.Id))
	} else {
		c.Send(responseByte)
	}
	c.logger.Debug("Socket server send data back", map[string]interface{}{
		"topic":      message.Topic,
		"id":         message.Id,
		"elapsed_ms": float64(time.Since(startTime).Nanoseconds()) / 1000000.0,
	})
}
