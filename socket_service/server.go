package socket_service

import (
	"gitlab.com/silenteer-oss/hestia/socket_service/configuration"
	"gitlab.com/silenteer-oss/hestia/socket_service/internal/app/socket"
	"gitlab.com/silenteer-oss/hestia/socket_service/internal/app/socket/manager"
	"net/http"
	"strings"
	"time"

	"github.com/spf13/viper"
	"gitlab.com/silenteer-oss/hestia/socket_service/api"
	"gitlab.com/silenteer-oss/hestia/socket_service/internal/app"
	"gitlab.com/silenteer-oss/titan"
)

func init() {
	viper.SetDefault("request.uri.4.validate", "/api/service/auth/validate")
	viper.SetDefault("jwt.cookie.names", "JWT,JWT_PATIENT")
	viper.SetDefault("duplicate.user.socket.allowed", false)
	viper.SetDefault(configuration.SocketResponseTimeout, 5*time.Second)
}

// InitServer to start call .Start()
// Default value:
// request.uri.4.validate: /api/service/auth/validate
// jwt.cookie.name: JWT
func InitServer() SocketService {
	authService := app.InitAuthService(
		titan.GetDefaultClient(),
		viper.GetString("request.uri.4.validate"),
		strings.Split(viper.GetString("jwt.cookie.names"), ","),
		titan.GetLogger())

	socketManager := manager.InitSocketManager(titan.GetLogger())
	socketService := socket.InitSocketService(socketManager, viper.GetBool("duplicate.user.socket.allowed"))
	socketNatRouter := api.NewSocketServiceRouter(socketService)

	socketNatServer := titan.NewServer("api.service.socket",
		titan.Routes(socketNatRouter.Register),
		titan.Queue(""),
	)

	return app.InitSocketGateWay(socketManager, socketNatServer, titan.GetLogger(), authService, app.GetHttpConfig())
}

type SocketService interface {
	Start()
	Stop()
	ServeHTTP(w http.ResponseWriter, r *http.Request)
}
