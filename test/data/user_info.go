package data

import (
	"github.com/google/uuid"
	infra "gitlab.com/silenteer-oss/hestia/infrastructure"
	"gitlab.com/silenteer-oss/titan"
)

func CreateUserInfo(c CareProviderInterface, userId uuid.UUID, role titan.Role, externalUserId titan.UUID) *titan.UserInfo {
	return &titan.UserInfo{
		ExternalUserId:  externalUserId,
		UserId:          titan.UUID(userId.String()),
		CareProviderId:  titan.UUID(c.GetId().String()),
		CareProviderKey: c.GetKey(),
		DeviceId:        c.GetDeviceId().String(),
		Role:            role,
		Attributes: map[string]interface{}{
			"device_id":         c.GetDeviceId().String(),
			"care_provider_id":  c.GetId().String(),
			"care_provider_key": c.GetKey(),
			"roles":             []string{role.String()},
			"sub":               c.GetDeviceId().String(),
			"nbf":               1590228713000,
			"iss":               "authentication-service",
			"iat":               1590228713000,
		},
	}
}

func CreateDoctorInfo(c CareProviderInterface, userId uuid.UUID) *titan.UserInfo {
	return &titan.UserInfo{
		ExternalUserId:  "",
		UserId:          titan.UUID(userId.String()),
		CareProviderId:  titan.UUID(c.GetId().String()),
		CareProviderKey: c.GetKey(),
		DeviceId:        c.GetDeviceId().String(),
		Role:            infra.CARE_PROVIDER_MEMBER,
		Attributes: map[string]interface{}{
			"device_id":         c.GetDeviceId().String(),
			"care_provider_id":  c.GetId().String(),
			"care_provider_key": c.GetKey(),
			"roles":             []string{infra.CARE_PROVIDER_MEMBER.String()},
			"sub":               c.GetDeviceId().String(),
			"nbf":               1590228713000,
			"iss":               "authentication-service",
			"iat":               1590228713000,
		},
	}
}

func NewAdminUserInfo(careProviderId uuid.UUID, careProviderKey string, adminUserId uuid.UUID) *titan.UserInfo {
	adminUserInfo := &titan.UserInfo{
		UserId:          titan.UUID(adminUserId.String()), //no one, does not exist
		Role:            infra.CARE_PROVIDER_ADMIN,
		CareProviderId:  titan.UUID(careProviderId.String()),
		CareProviderKey: careProviderKey,
	}

	return adminUserInfo
}
