package fixtures_test

import (
	"os"
	"testing"

	"gitlab.com/silenteer-oss/hestia/test/context"

	"github.com/stretchr/testify/assert"
	infra "gitlab.com/silenteer-oss/hestia/infrastructure"

	"github.com/stretchr/testify/require"

	"gitlab.com/silenteer-oss/titan"
)

type testCacheData struct {
	Value int
}

var ctx *titan.Context
var cacheObject testCacheData
var cacheObjectKey string

func TestMain(m *testing.M) {
	cacheObject = testCacheData{10}
	cacheObjectKey = "CACHE_OBJECT_KEY"
	ctx = titan.NewBackgroundContext()
	exitVal := m.Run()
	os.Exit(exitVal)
}

func Test_can_get_and_set_reference_object_successfully(t *testing.T) {
	context.AddReference(ctx, cacheObjectKey, cacheObject)
	actualCache := context.GetReference(ctx, cacheObjectKey)
	require.NotNil(t, actualCache)
	if actualCacheObject, ok := actualCache.(testCacheData); ok {
		assert.Equal(t, cacheObject.Value, actualCacheObject.Value)
	} else {
		assert.Failf(t, "Could not get value from fixture cache", "")
	}
}

func Test_cannot_get_un_existing_reference_object(t *testing.T) {
	actualCache := context.GetReference(ctx, "Not_existing_object_key")
	require.Nil(t, actualCache)
}

func Test_object_must_be_cached_under_its_care_provider(t *testing.T) {
	adminUserInfo := &titan.UserInfo{
		Role: infra.CARE_PROVIDER_ADMIN,
	}
	careProviderCtx := ctx.WithValue(titan.XUserInfo, adminUserInfo)
	context.AddReference(careProviderCtx, cacheObjectKey, cacheObject)

	subCtx := careProviderCtx.WithValue("sub_key", "")
	cacheKeyUnderCareProvider := context.GetCareProviderCacheKey(subCtx, cacheObjectKey)

	actualCache := context.GetReference(subCtx, cacheKeyUnderCareProvider)
	require.NotNil(t, actualCache)
}
