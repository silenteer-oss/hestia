package fixtures

import (
	"emperror.dev/errors"
	"gitlab.com/silenteer-oss/hestia/test/context"
	"gitlab.com/silenteer-oss/hestia/test/data"
	"gitlab.com/silenteer-oss/titan"
)

type ReferenceInterface interface {
	AddReference(ctx *titan.Context, key string, obj interface{})
	GetReference(ctx *titan.Context, key string) interface{}
	GetCareProvider(ctx *titan.Context, key string) data.CareProviderInterface
	GetAllCareProviders(ctx *titan.Context) []data.CareProviderInterface
	GetPatient(ctx *titan.Context, key string) data.PatientInterface
	GetEmployee(ctx *titan.Context, key string) data.EmployeeInterface
	GetDoctor(ctx *titan.Context, key string) data.DoctorInterface
	GetNurse(ctx *titan.Context, key string) data.NurseInterface
	GetCareProviderInCurrentContext(ctx *titan.Context) (data.CareProviderInterface, error)
}

type AbstractReferenceFixture struct {
}

func (a *AbstractReferenceFixture) GetPatient(ctx *titan.Context, key string) data.PatientInterface {
	return GetPatient(ctx, key)
}

func (a *AbstractReferenceFixture) GetEmployee(ctx *titan.Context, key string) data.EmployeeInterface {
	return GetEmployee(ctx, key)
}

func (a *AbstractReferenceFixture) GetDoctor(ctx *titan.Context, key string) data.DoctorInterface {
	return GetDoctor(ctx, key)
}

func (a *AbstractReferenceFixture) GetNurse(ctx *titan.Context, key string) data.NurseInterface {
	return GetNurse(ctx, key)
}

func (a *AbstractReferenceFixture) GetCareProvider(ctx *titan.Context, key string) data.CareProviderInterface {
	return GetCareProvider(ctx, key)
}

func (a *AbstractReferenceFixture) GetAllCareProviders(ctx *titan.Context) []data.CareProviderInterface {
	return GetAllCareProviders(ctx)
}

func (a *AbstractReferenceFixture) AddReference(ctx *titan.Context, key string, obj interface{}) {
	context.AddReference(ctx, key, obj)
}

func (a *AbstractReferenceFixture) GetReference(ctx *titan.Context, key string) interface{} {
	return context.GetReference(ctx, key)
}

func (a *AbstractReferenceFixture) GetCareProviderInCurrentContext(ctx *titan.Context) (data.CareProviderInterface, error) {
	userInfo := ctx.UserInfo()
	if userInfo == nil {
		return nil, errors.New("MvzFixture must be run under care provider context")
	}
	careProvider := a.GetCareProvider(ctx, userInfo.CareProviderId.String())
	if careProvider == nil {
		return nil, errors.New("missing care provider in the context")
	}
	return careProvider, nil
}

func newAbstractReference() ReferenceInterface {
	return &AbstractReferenceFixture{}
}
