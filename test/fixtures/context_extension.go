package fixtures

import (
	"strings"

	"gitlab.com/silenteer-oss/hestia/test/context"

	"gitlab.com/silenteer-oss/hestia/test/data"

	"github.com/google/uuid"

	"gitlab.com/silenteer-oss/titan"
)

func GetCareProvider(ctx *titan.Context, key string) data.CareProviderInterface {
	cache := context.GetGlobalCache(ctx)
	for k, val := range cache.Data {
		if !strings.HasSuffix(k, context.FixtureCacheKeyPrefix+key) {
			continue
		}
		if c, ok := val.(data.CareProviderInterface); ok {
			return c
		}
	}
	return nil
}

func GetAllCareProviders(ctx *titan.Context) []data.CareProviderInterface {
	careProviders := make([]data.CareProviderInterface, 0)
	cache := context.GetGlobalCache(ctx)

	for _, val := range cache.Data {
		if c, ok := val.(data.CareProviderInterface); ok {
			careProviders = append(careProviders, c)
		}
	}
	return careProviders
}
func GetCareProviderById(ctx *titan.Context, id uuid.UUID) data.CareProviderInterface {
	for _, c := range GetAllCareProviders(ctx) {
		if c.GetId() == id {
			return c
		}
	}
	return nil
}

func GetPatient(ctx *titan.Context, key string) data.PatientInterface {
	in := context.GetReference(ctx, key)
	if in == nil {
		return nil
	}

	if p, ok := in.(data.PatientInterface); ok {
		return p
	}
	return nil
}
func GetEmployee(ctx *titan.Context, key string) data.EmployeeInterface {
	in := context.GetReference(ctx, key)
	if in == nil {
		return nil
	}
	if em, ok := in.(data.EmployeeInterface); ok {
		return em
	}
	return nil
}

func GetDoctor(ctx *titan.Context, key string) data.DoctorInterface {
	in := context.GetReference(ctx, key)
	if in == nil {
		return nil
	}
	if d, ok := in.(data.DoctorInterface); ok {
		return d
	}
	return nil
}

func GetNurse(ctx *titan.Context, key string) data.NurseInterface {
	in := context.GetReference(ctx, key)
	if in == nil {
		return nil
	}
	if n, ok := in.(data.NurseInterface); ok {
		return n
	}
	return nil
}
