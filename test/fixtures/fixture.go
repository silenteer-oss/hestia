package fixtures

import (
	"gitlab.com/silenteer-oss/titan"
)

type FixtureInterface interface {
	//DependentInterface
	ReferenceInterface
	SetUp(ctx *titan.Context) error
	TearDown(ctx *titan.Context) error
}

// all dependent fixtures will be run under this context
type ContextSwitcherInterface interface {
	SwitchContext(ctx *titan.Context) *titan.Context
}
