package fixtures

import (
	"fmt"

	"gitlab.com/silenteer-oss/hestia/test/db/history"

	"github.com/brianvoe/gofakeit/v5"

	"gitlab.com/silenteer-oss/hestia/test/data"

	"emperror.dev/errors"
	"gitlab.com/silenteer-oss/titan"
)

type CareProviderFixture struct {
	ContextSwitcherInterface
	AbstractDependentFixture
	AbstractReferenceFixture
	careProvider data.CareProviderInterface
	alias        string
	seed         *int64
}

func (f *CareProviderFixture) GetSelfCareProvider() data.CareProviderInterface {
	return f.careProvider
}

func (f *CareProviderFixture) GetAlias() string {
	return f.alias
}

func (f *CareProviderFixture) GetSeed() *int64 {
	return f.seed
}

func newCareProviderFixture() FixtureInterface {
	return &CareProviderFixture{}
}

func NewCareProviderFixture(careProvider data.CareProviderInterface, alias string, dependFixtures ...FixtureInterface) *CareProviderFixture {
	f := &CareProviderFixture{
		careProvider: careProvider,
		alias:        alias,
	}
	if len(dependFixtures) > 0 {
		f.AddDependencies(dependFixtures...)
	}

	return f
}

func NewCareProviderFixtureWithSeed(careProvider data.CareProviderInterface, alias string, seed int64, dependFixtures ...FixtureInterface) *CareProviderFixture {
	f := &CareProviderFixture{
		careProvider: careProvider,
		alias:        alias,
		seed:         &seed,
	}
	if len(dependFixtures) > 0 {
		f.AddDependencies(dependFixtures...)
	}

	return f
}

func (f *CareProviderFixture) SetUp(ctx *titan.Context) error {
	var err error
	ctx.Logger().Debug("setting up care provider ......", map[string]interface{}{"id": f.careProvider.GetId()})

	if f.seed != nil {
		gofakeit.Seed(*f.seed)
	}

	if f.careProvider == nil {
		return nil
	}

	err = f.careProvider.SetUpData()
	if err != nil {
		return errors.WithMessage(err, fmt.Sprintf("error on creating care provider id=%s", f.careProvider.GetId()))
	}
	if f.alias != "" {
		f.AddReference(ctx, f.alias, f.careProvider)
	}

	f.AddReference(ctx, f.careProvider.GetId().String(), f.careProvider)

	err = history.LogExecutedCareProvider(f.careProvider.GetId(), f.careProvider.GetHashedId())
	if err != nil {
		return errors.WithMessage(err, fmt.Sprintf("error on  logging care provider history=%s", f.careProvider.GetId()))
	}
	return nil
}

func (f *CareProviderFixture) TearDown(ctx *titan.Context) error {
	var err error
	if f.careProvider != nil {
		err = f.careProvider.CleanUpData()
		if err != nil {
			return errors.WithMessage(err, fmt.Sprintf("error on clean up care provider data id=%s", f.careProvider.GetId()))
		}
		err = history.DeleteExecutedCareProvider(f.careProvider.GetId())
		if err != nil {
			return errors.WithMessage(err, fmt.Sprintf("error on delete care provider history id=%s", f.careProvider.GetId()))
		}
	}
	return nil
}

func (f *CareProviderFixture) SwitchContext(ctx *titan.Context) *titan.Context {
	return f.careProvider.SwitchToAdminContext(ctx)
}
