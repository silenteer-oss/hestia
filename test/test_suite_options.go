package test

import (
	"fmt"
	"os"

	"gitlab.com/silenteer-oss/titan"

	"gitlab.com/silenteer-oss/titan/test"

	"github.com/stretchr/testify/suite"
	"gitlab.com/silenteer-oss/hestia/test/fixtures"
)

type Option func(*Options) error

type Options struct {
	fixtures           []fixtures.FixtureInterface
	testServers        test.TestServersInterface
	enableSocketServer bool
}

func Fixtures(fixtures []fixtures.FixtureInterface) Option {
	return func(o *Options) error {
		o.fixtures = fixtures
		return nil
	}
}

func TestServers(servers test.TestServersInterface) Option {
	return func(o *Options) error {
		o.testServers = servers
		return nil
	}
}

func EnableSocketServer(value bool) Option {
	return func(o *Options) error {
		o.enableSocketServer = value
		return nil
	}
}

func NewTestSuite(options ...Option) TestSuite {
	opts := Options{}

	// merge options with user define
	for _, opt := range options {
		if opt != nil {
			if err := opt(&opts); err != nil {
				fmt.Printf("Nats server creation error: %+v\n ", err)
				os.Exit(1)
			}
		}
	}

	return TestSuite{
		Suite:              suite.Suite{},
		fixtures:           opts.fixtures,
		testServers:        opts.testServers,
		enableSocketServer: opts.enableSocketServer,
	}
}

func (suite *TestSuite) SetTestServers(testServers test.TestServersInterface) *TestSuite {
	suite.testServers = testServers
	return suite
}

func (suite *TestSuite) SetFixtures(fixtures ...fixtures.FixtureInterface) *TestSuite {
	suite.fixtures = append(suite.fixtures, fixtures...)
	return suite
}

func (suite *TestSuite) SetEnableSocketServer(value bool) *TestSuite {
	suite.enableSocketServer = value
	return suite
}

func (suite *TestSuite) GetFixtures() []fixtures.FixtureInterface {
	return suite.fixtures
}

func (suite *TestSuite) GetFixtureContext() *titan.Context {
	return suite.fixturesContext
}
