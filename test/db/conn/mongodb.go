package conn

import (
	"context"
	"os"
	"os/signal"
	"sync"
	"syscall"

	"go.mongodb.org/mongo-driver/mongo/readpref"

	"github.com/spf13/viper"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

var mux sync.Mutex
var defaultClient *mongo.Client

func init() {
	viper.SetDefault("mongodb.uri", "mongodb://127.0.0.1:27017")
}

func GetConnection(ctx context.Context) (*mongo.Client, error) {
	var err error
	if defaultClient != nil {
		return defaultClient, nil
	}
	mux.Lock()
	mongoUri := viper.GetString("mongodb.uri")
	options := options.Client().ApplyURI(mongoUri)
	fa := false
	options.RetryWrites = &fa
	defaultClient, err = mongo.Connect(ctx, options)
	if err == nil {
		err = defaultClient.Ping(ctx, readpref.Primary())
	}

	go func() {
		done := make(chan os.Signal, 1)
		signal.Notify(done, syscall.SIGINT, syscall.SIGTERM, os.Interrupt)
		// close connection on exit
		<-done
		defaultClient.Disconnect(ctx)
		defaultClient = nil
	}()
	mux.Unlock()

	return defaultClient, err
}
