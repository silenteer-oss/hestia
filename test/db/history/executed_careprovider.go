package history

import (
	"context"
	"fmt"
	"time"

	"gitlab.com/silenteer-oss/hestia/test/db/conn"

	"emperror.dev/errors"

	"go.mongodb.org/mongo-driver/bson/primitive"

	"go.mongodb.org/mongo-driver/bson"

	bson2 "gitlab.com/silenteer-oss/hestia/db_gateway_service/pkg/mongodb/parser/bson"

	"github.com/google/uuid"

	"go.mongodb.org/mongo-driver/mongo/options"
)

// Model definitions
type ExecutedCareProvider struct {
	Id       uuid.UUID `bson:"_id"`
	HashedId string    `bson:"hashedId"`
}

const DatabaseName = "test_history"
const CareProviderCollectionName = "executed_careprovider"
const CareProviderFieldId = "_id"
const CareProviderFieldHashId = "hashedId"

func LogExecutedCareProvider(id uuid.UUID, hashedId string) error {
	ctx, cancel := context.WithTimeout(context.Background(), 20*time.Second)
	defer cancel()

	client, err := conn.GetConnection(ctx)
	if err != nil {
		return err
	}

	//defer client.Disconnect(ctx)

	col := client.Database(DatabaseName).Collection(CareProviderCollectionName)

	filter, _ := bson2.Byte(bson.D{
		{
			Key:   CareProviderFieldId,
			Value: id,
		},
	})

	update := bson.M{
		"$set": bson.M{CareProviderFieldHashId: hashedId},
	}
	upsert := true
	after := options.After
	opt := options.FindOneAndUpdateOptions{
		ReturnDocument: &after,
		Upsert:         &upsert,
	}
	result := col.FindOneAndUpdate(ctx, filter, update, &opt)
	return result.Err()
}

func ListExecutedCareProviders() ([]ExecutedCareProvider, error) {
	var result []ExecutedCareProvider
	ctx, cancel := context.WithTimeout(context.Background(), 20*time.Second)
	defer cancel()
	client, err := conn.GetConnection(ctx)
	if err != nil {
		return nil, err
	}

	//defer client.Disconnect(ctx)

	col := client.Database(DatabaseName).Collection(CareProviderCollectionName)

	cursor, err := col.Find(ctx, bson.M{})
	if err != nil {
		return result, err
	}

	defer cursor.Close(ctx)
	for cursor.Next(ctx) {
		var episode bson.M
		if err = cursor.Decode(&episode); err != nil {
			return result, err
		}

		id, idOk := episode[CareProviderFieldId].(primitive.Binary)
		hashId, hashOk := episode[CareProviderFieldHashId].(string)

		if idOk && hashOk {
			legacyId, _ := bson2.ConvertToUUIDLegacy(id.Data, bson2.UUIDLegacy)
			result = append(result, ExecutedCareProvider{
				Id:       legacyId,
				HashedId: hashId,
			})
		}
	}
	return result, nil
}

func DeleteExecutedCareProvider(id uuid.UUID) error {
	ctx, cancel := context.WithTimeout(context.Background(), 20*time.Second)
	defer cancel()

	client, err := conn.GetConnection(ctx)
	if err != nil {
		return err
	}
	//defer client.Disconnect(ctx)

	col := client.Database(DatabaseName).Collection(CareProviderCollectionName)

	filter, _ := bson2.Byte(bson.D{
		{
			Key:   CareProviderFieldId,
			Value: id,
		},
	})
	_, err = col.DeleteMany(ctx, filter)
	if err != nil {
		return errors.WithMessage(err, fmt.Sprintf("Delete careProviderId error  %s.%s", DatabaseName, CareProviderCollectionName))
	}

	return nil
}
