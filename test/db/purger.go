package db

import (
	"context"
	"fmt"
	"strings"
	"time"

	"gitlab.com/silenteer-oss/hestia/test/db/conn"

	"gitlab.com/silenteer-oss/hestia/infrastructure/util"

	"gitlab.com/silenteer-oss/hestia/test/db/history"

	"emperror.dev/errors"
	"github.com/google/uuid"
	bson2 "gitlab.com/silenteer-oss/hestia/db_gateway_service/pkg/mongodb/parser/bson"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
)

const GlobalKey = "asdwadsdwajkhfkdsfhjkehjkhefa"

func CleanUpData(careProviderId uuid.UUID, careProviderHashedId string) error {
	if careProviderHashedId == "" {
		careProviderHashedId, _ = util.EncryptPBE(careProviderId.String(), GlobalKey)
	}

	ctx, cancel := context.WithTimeout(context.Background(), 20*time.Second)
	defer cancel()

	client, err := conn.GetConnection(ctx)
	if err != nil {
		return err
	}

	//defer client.Disconnect(ctx)

	databases, err := client.ListDatabaseNames(ctx, bson.D{})
	if err != nil {
		return err
	}

	for _, databaseName := range databases {
		// do not delete system db
		if strings.EqualFold(databaseName, "system") ||
			strings.EqualFold(databaseName, "config") ||
			strings.EqualFold(databaseName, "admin") ||
			strings.EqualFold(databaseName, "local") {
			continue
		}

		collections, err := client.Database(databaseName).ListCollectionNames(ctx, bson.D{})
		if err != nil {
			return errors.WithMessage(err, fmt.Sprintf("list collection error %s", databaseName))
		}

		for _, collectionName := range collections {
			if strings.HasSuffix(collectionName, "."+careProviderId.String()) {
				err = DropCollection(ctx, client, databaseName, collectionName)
			} else if !strings.Contains(collectionName, ".") {
				err = DropRecordsContainCareProviderId(ctx, client, databaseName, collectionName, careProviderId, careProviderHashedId)
			}
			if err != nil {
				return err
			}
		}
	}

	return nil
}

func DropCollection(ctx context.Context, client *mongo.Client, databaseName, collectionName string) error {
	var err error
	collection := client.Database(databaseName).Collection(collectionName)
	err = collection.Drop(ctx)
	if err != nil {
		return errors.WithMessage(err, fmt.Sprintf("Delete collection error  %s.%s", databaseName, collectionName))
	}

	return nil
}

func DropRecordsContainCareProviderId(ctx context.Context,
	client *mongo.Client,
	databaseName, collectionName string,
	careProviderId uuid.UUID, careProviderHashedId string) error {

	collection := client.Database(databaseName).Collection(collectionName)
	filter, _ := bson2.Byte(bson.D{
		{
			Key:   "careProviderId",
			Value: careProviderId,
		},
	})
	_, err := collection.DeleteMany(ctx, filter)
	if err != nil {
		return errors.WithMessage(err, fmt.Sprintf("Delete careProviderId error  %s.%s", databaseName, collectionName))
	}

	_, err = collection.DeleteMany(ctx, bson.M{"careProviderHashedId": bson.M{"$eq": careProviderHashedId}})
	if err != nil {
		return errors.WithMessage(err, fmt.Sprintf("Delete careProviderHashedId error  %s.%s", databaseName, collectionName))
	}

	return nil
}

func DeleteAllExecutedCareProviders() error {
	cares, err := history.ListExecutedCareProviders()
	if err != nil {
		return err
	}
	for _, c := range cares {
		err := CleanUpData(c.Id, c.HashedId)
		if err != nil {
			return err
		}
		err = history.DeleteExecutedCareProvider(c.Id)
		if err != nil {
			return err
		}
	}
	return nil
}

func ParseUUID(value string) uuid.UUID {
	id, err := uuid.Parse(value)
	if err != nil {
		fmt.Println("Parsing UUID error ", err)
	}
	return id
}
