package runner

import (
	"fmt"
	"reflect"

	"gitlab.com/silenteer-oss/hestia/test/fixtures"
	"gitlab.com/silenteer-oss/titan"

	"emperror.dev/errors"
)

func SetUp(ctx *titan.Context, fxtures ...fixtures.FixtureInterface) error {
	// to fix circle dependency
	executedFixtures := map[fixtures.FixtureInterface]bool{}
	for _, f := range fxtures {
		err := doSetUp(ctx, f, executedFixtures)
		if err != nil {
			return err
		}
	}
	return nil
}

func doSetUp(ctx *titan.Context, f fixtures.FixtureInterface, executedFixture map[fixtures.FixtureInterface]bool) error {
	if f == nil || (executedFixture[f] == true) {
		return nil
	}
	logger := ctx.Logger()
	logger.Debug(fmt.Sprintf("Begin to up fixture %T", f))
	err := f.SetUp(ctx)
	if err != nil {
		return errors.WithMessage(err, fmt.Sprintf("Error on up fixture %T", f))
	}
	logger.Debug(fmt.Sprintf("End of up  fixture %T", f))

	executedFixture[f] = true

	newCtx := switchContextIfAny(ctx, f)

	if dependency, ok := f.(fixtures.DependentInterface); ok {
		for _, d := range dependency.GetDependencies() {
			err = doSetUp(newCtx, d, executedFixture)
			if err != nil {
				return err
			}
		}
	}

	return nil
}

func TearDown(ctx *titan.Context, fxtures ...fixtures.FixtureInterface) error {
	executedFixtures := map[fixtures.FixtureInterface]bool{}
	for i := len(fxtures) - 1; i >= 0; i-- {
		err := doTearDown(ctx, fxtures[i], executedFixtures)
		if err != nil {
			return err
		}
	}
	return nil
}

func doTearDown(ctx *titan.Context, f fixtures.FixtureInterface, executedFixture map[fixtures.FixtureInterface]bool) error {
	if f == nil || (executedFixture[f] == true) {
		return nil
	}
	logger := ctx.Logger()
	logger.Debug(fmt.Sprintf("Begin to down fixture %T", f))
	err := f.TearDown(ctx)
	if err != nil {
		return errors.WithMessage(err, fmt.Sprintf("Error on down fixture %T", f))
	}
	logger.Debug(fmt.Sprintf("End of down  fixture %T", f))

	executedFixture[f] = true

	newCtx := switchContextIfAny(ctx, f)

	if dependency, ok := f.(fixtures.DependentInterface); ok {
		for _, d := range dependency.GetDependencies() {
			err = doTearDown(newCtx, d, executedFixture)
			if err != nil {
				return err
			}
		}
	}
	return nil
}

func Reset(ctx *titan.Context, fixtures ...fixtures.FixtureInterface) error {
	err := TearDown(ctx, fixtures...)
	if err != nil {
		return err
	}
	err = SetUp(ctx, fixtures...)
	return err
}

func getFixtureName(f fixtures.FixtureInterface) string {
	if f == nil {
		return ""
	}
	methodFinder := reflect.TypeOf(f)
	fixtureName := methodFinder.Elem().Name()
	return fixtureName
}

func switchContextIfAny(ctx *titan.Context, f fixtures.FixtureInterface) *titan.Context {
	newCtx := ctx
	if switcher, ok := f.(fixtures.ContextSwitcherInterface); ok {
		newCtx = switcher.SwitchContext(ctx)
	}

	return newCtx
}
