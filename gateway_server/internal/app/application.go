package app

import (
	"fmt"
	"os"

	"github.com/spf13/viper"
	"gitlab.com/silenteer-oss/titan"
)

func init() {
	viper.SetDefault("Http.GetwayPort", 8096)
	viper.SetDefault("patient_api_domain", "patient.local")
	viper.SetDefault("root_url", "http://patient.local")
}

type HttpConfig struct {
	GetwayPort int
}

type DomainConfig struct {
	RootUrl          string
	PatientApiDomain string
}

func GetHttpConfig() *HttpConfig {
	var config HttpConfig
	err := viper.UnmarshalKey("Http", &config)
	if err != nil {
		fmt.Printf("Unmarshal http config error %+v\n", err)
		os.Exit(1)
	}
	fmt.Printf("Configuration gateway_server: %v", config)
	return &config
}

func GetDomainConfig() *DomainConfig {
	config := DomainConfig{
		RootUrl:          viper.GetString("root_url"),
		PatientApiDomain: viper.GetString("patient_api_domain"),
	}
	return &config
}

func NewServer() *gatewayServer {
	return NewGatewayServer(
		titan.GetLogger(),
		titan.GetDefaultClient(),
		GetHttpConfig(),
		GetDomainConfig(),
	)
}
