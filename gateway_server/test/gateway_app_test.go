package test

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"testing"
	"time"

	"github.com/stretchr/testify/require"

	"github.com/stretchr/testify/assert"

	"gitlab.com/silenteer-oss/titan"

	"gitlab.com/silenteer-oss/hestia/gateway_server/internal/app"
)

type HalloResponse struct {
	Value string `json:"value"`
}

var defaultHalloResponse = &HalloResponse{Value: "hallo me"}

func NewTestServer() *titan.Server {
	return titan.NewServer("api.app.testservice",
		titan.Routes(func(r titan.Router) {
			r.RegisterJson("GET", "/api/app/testservice/hallo", func(c *titan.Context) (*HalloResponse, error) {
				return defaultHalloResponse, nil
			})
		}),
	)
}

func TestMain(m *testing.M) {
	gatewayServer := app.NewServer()
	testServer := NewTestServer()

	go func() {
		gatewayServer.Start()
	}()

	go func() {
		testServer.Start()
	}()

	time.Sleep(1 * time.Second)

	exitVal := m.Run()

	gatewayServer.Stop()
	testServer.Stop()

	time.Sleep(1 * time.Microsecond)
	os.Exit(exitVal)
}

func TestGetHallo(t *testing.T) {
	resp, err := http.Get(fmt.Sprintf("http://localhost:%d/api/app/testservice/hallo",
		app.GetHttpConfig().GetwayPort))
	require.NoError(t, err)
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	require.NoError(t, err)

	var resData HalloResponse

	err = json.Unmarshal(body, &resData)
	require.NoError(t, err)

	assert.Equal(t, defaultHalloResponse.Value, resData.Value)
}
