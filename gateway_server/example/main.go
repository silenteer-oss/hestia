package main

import "gitlab.com/silenteer-oss/hestia/gateway_server/internal/app"

func main() {
	gatewayServer := app.NewServer()
	gatewayServer.Start()
}
