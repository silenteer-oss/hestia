package gateway_server

import (
	"gitlab.com/silenteer-oss/hestia/gateway_server/internal/app"
	"net/http"
)

// InitGatewayServer
// Default config param:
// Http.Port: 8096
func InitGatewayServer() GatewayServer {
	return app.NewServer()
	//defer func() {
	//	appServer.Stop()
	//}()
	//appServer.Start()

}

type GatewayServer interface {
	Start()
	Stop()
	ServeHTTP(w http.ResponseWriter, r *http.Request)
}