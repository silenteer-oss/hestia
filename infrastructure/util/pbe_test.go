package util

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestHash(t *testing.T) {
	hashValue := Hash("c6198180-6981-4ecd-8539-cdbb58fedcbe", "syWzDdWgtRNosrHvxAAaWu")
	assert.Equal(t, "D8dEpQv9Dl8vdcWevUgjHZAkIl6DdxtDB6FlYnLh+lkdyCBRR9HEwVMb7HGQ7yhpNQASXTUM3hxNKe1GzTOUGw==", hashValue)
}

// TestDecryptPBE Data get from java project.
func TestDecryptPBE(t *testing.T) {

	result, err := DecryptPBE("A1mmyKA9My6RYcE27tkHw6iVfwhs1wwk/En2lsa6QsQx8Vzi1LELczMVh2i4q/6U45NaG0mYWX+xUp9TEgHD7Q49zIOr4BVoeGyWl4WKiJ8=", "syWzDdWgtRNosrHvxAAaWu")
	assert.Nil(t, err)
	assert.Equal(t, "5c96ea9f-239c-4dba-b514-fd3531366c99", result)

	result, err = DecryptPBE("dz0ZJpXa7OrR/ps3y3aKBamZTl8egYEok0JWyGLo1zE/KKfhzA5sGICG1hqrc9u/0YjDVlRivcjeFFfL2HTyb/nXP0MJcy9oTyeyZDmgVVc=", "syWzDdWgtRNosrHvxAAaWu")
	assert.Nil(t, err)
	assert.Equal(t, "ccd6a490-46c8-411b-bf2c-c273b0e5d714", result)

	result, err = DecryptPBE("CxyK6lc916135TANnWi1poEdT1JKO8Wurjhm3aiepuLU/0WdAe20Dbsjtz/FX4zwtaouKbnn93nGtIdyJO4Wfpc7XTWXF8UJmaOKl6Uyi6I=", "syWzDdWgtRNosrHvxAAaWu")
	assert.Nil(t, err)
	assert.Equal(t, "5c96ea9f-239c-4dba-b514-fd3531366c99", result)
}

func TestEncryptPBE(t *testing.T) {
	data := "5c96ea9f-239c-4dba-b514-fd3531366c99"
	encrypted, err := EncryptPBE(data, "syWzDdWgtRNosrHvxAAaWu")
	assert.Nil(t, err)
	decrypted, err := DecryptPBE(encrypted, "syWzDdWgtRNosrHvxAAaWu")
	assert.Nil(t, err)
	assert.Equal(t, data, decrypted)
}
