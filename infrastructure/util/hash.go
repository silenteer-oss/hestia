package util

import (
	"fmt"
	"golang.org/x/crypto/sha3"
)

func OneWayHash(data []byte) string {
	h := make([]byte, 64)
	sha3.ShakeSum256(h, data)
	return fmt.Sprintf("%x", h)
}
