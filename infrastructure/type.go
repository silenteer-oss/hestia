package infrastructure

import "gitlab.com/silenteer-oss/titan"

const (
	SYS_ADMIN                       titan.Role = "SYS_ADMIN"
	CARE_PROVIDER_ADMIN             titan.Role = "CARE_PROVIDER_ADMIN"
	CARE_PROVIDER_MEMBER            titan.Role = "CARE_PROVIDER_MEMBER"            // doctor or staff
	CARE_PROVIDER_MEMBER_PRE_SWITCH titan.Role = "CARE_PROVIDER_MEMBER_PRE_SWITCH" // use for case there is only token. practice member has not yet login to have session
	PATIENT                         titan.Role = "PATIENT"
	PATIENT_PRE_SWITCH              titan.Role = "PATIENT_PRE_SWITCH"
	SYSTEM                          titan.Role = "SYSTEM" // example: cron job
	REGISTERING_USER                titan.Role = "REGISTERING_USER"
	APPOINTMENT_BOOKER              titan.Role = "APPOINTMENT_BOOKER"
)

const OneMinuteInMilliSecond int64 = 60000

// it's a mockup used please dont delete
type Empty struct {
}
