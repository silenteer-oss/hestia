module gitlab.com/silenteer-oss/hestia

go 1.16

require (
	emperror.dev/errors v0.7.0
	github.com/brianvoe/gofakeit/v5 v5.9.2
	github.com/golang/protobuf v1.4.2
	github.com/google/go-cmp v0.5.2
	github.com/google/uuid v1.1.1
	github.com/gorilla/websocket v1.4.2
	github.com/iancoleman/strcase v0.0.0-20191112232945-16388991a334
	github.com/pkg/errors v0.9.1
	github.com/spf13/viper v1.7.1
	github.com/stretchr/testify v1.6.1
	gitlab.com/silenteer-oss/goff v1.1.5
	gitlab.com/silenteer-oss/titan v1.0.69
	go.mongodb.org/mongo-driver v1.4.3
	golang.org/x/crypto v0.0.0-20210314154223-e6e6c4f2bb5b
	logur.dev/logur v0.16.2
)
