package authz

import (
	"encoding/binary"

	bson2 "gitlab.com/silenteer-oss/hestia/db_gateway_service/pkg/mongodb/parser/bson"
	"gitlab.com/silenteer-oss/titan"
	"go.mongodb.org/mongo-driver/bson"
)

func AddCareProviderConditionToFilter(filter []byte, field string, value interface{}) ([]byte, error) {
	d := bson.M{field: value}
	return appendFilter(filter, d)
}

func AddCareConsumerConditionToFilter(filter []byte, field string, userId titan.UUID, externalUserid titan.UUID) ([]byte, error) {
	d := bson.M{"$or": bson.D{
		{Key: field, Value: parseUUID(userId.String())},
		{Key: field, Value: parseUUID(externalUserid.String())},
	}}
	return appendFilter(filter, d)
}

func appendFilter(filter []byte, cond bson.M) ([]byte, error) {

	dInByte, _ := bson2.Byte(cond)

	if len(filter) == 0 {
		return dInByte, nil
	}

	ret := make([]byte, 0, 1024)
	ret = append(ret, filter[:len(filter)-1]...)
	ret = append(ret, dInByte[4:]...)

	// set bson size
	lengthByte := make([]byte, 4)
	binary.LittleEndian.PutUint32(lengthByte, uint32(len(filter)-5+len(dInByte)-5+5))
	for l := 0; l < len(lengthByte); l++ {
		ret[l] = lengthByte[l]
	}

	var raw bson.Raw
	bson.Unmarshal(ret, &raw)

	return ret, nil
}
