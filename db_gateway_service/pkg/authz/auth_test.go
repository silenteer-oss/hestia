package authz_test

import (
	"context"
	"fmt"
	"os"
	"testing"
	"time"

	infra "gitlab.com/silenteer-oss/hestia/infrastructure"

	"github.com/stretchr/testify/assert"

	"gitlab.com/silenteer-oss/hestia/db_gateway_service/pkg/authz"
	bson2 "gitlab.com/silenteer-oss/hestia/db_gateway_service/pkg/mongodb/parser/bson"

	"github.com/brianvoe/gofakeit/v5"

	"emperror.dev/errors"

	"github.com/stretchr/testify/require"

	"go.mongodb.org/mongo-driver/bson"

	"gitlab.com/silenteer-oss/hestia/db_gateway_service/api/mongodb"

	"gitlab.com/silenteer-oss/hestia/db_gateway_service/api/dbgateway"

	"github.com/google/uuid"
	"github.com/spf13/viper"
	"gitlab.com/silenteer-oss/hestia/db_gateway_service/internal/service"
	"gitlab.com/silenteer-oss/titan"
)

type Db struct {
	AuthorClient  mongodb.IDBClient
	PostClient    mongodb.IDBClient
	UserClient    mongodb.IDBClient
	ArticleClient mongodb.IDBClient
	Context1      *titan.Context
	Context2      *titan.Context
}

type Author struct {
	Id             *uuid.UUID `json:"_id,omitempty" bson:"_id,omitempty"`
	Name           string     `json:"name" bson:"name"`
	Phone          string     `json:"phone" bson:"phone"`
	CareProviderId *uuid.UUID `json:"careProviderId" bson:"careProviderId"`
}

type Post struct {
	Id             *uuid.UUID `json:"_id,omitempty" bson:"_id,omitempty"`
	Title          string     `json:"title" bson:"title"`
	Description    string     `json:"description" bson:"description"`
	AuthorId       *uuid.UUID `json:"authorId" bson:"authorId"`
	CareProviderId *uuid.UUID `json:"careProviderId" bson:"careProviderId"`
}

type User struct {
	Id             *uuid.UUID `json:"_id,omitempty" bson:"_id,omitempty"`
	Name           string     `json:"name" bson:"name"`
	Phone          string     `json:"phone" bson:"phone"`
	CareProviderId *uuid.UUID `json:"careProviderId" bson:"careProviderId"`
}

type Article struct {
	Id             *uuid.UUID `json:"_id,omitempty" bson:"_id,omitempty"`
	Title          string     `json:"title" bson:"title"`
	Description    string     `json:"description" bson:"description"`
	UserId         *uuid.UUID `json:"userId" bson:"authorId"`
	CareProviderId *uuid.UUID `json:"careProviderId" bson:"careProviderId"`
}

const (
	careProviderId1       = "2800bfb4-c753-44e9-85f8-54a09d6a5825"
	careProviderId2       = "6216e2a5-13eb-4b30-b584-41a7a85ad610"
	PostCollectionName    = "post"
	AuthorCollectionName  = "author"
	UserCollectionName    = "users"
	ArticleCollectionName = "article"

	DatabaseName = "test_db"
)

var db Db

func init() {
}

func makeUUID() *uuid.UUID {
	id := uuid.New()
	return &id
}

func TestFind(t *testing.T) {
	err := prepareData()
	require.Nil(t, err)

	var response []Author
	db.AuthorClient.Find(db.Context1, bson.D{}, &response)
	assert.Equal(t, 5, len(response))

}

func TestMain(m *testing.M) {
	viper.SetDefault("mongodb.uri", "mongodb://127.0.0.1:27017/?retryWrites=false")
	viper.SetDefault("nats.subject", "api.service.db-gateway")
	natClient := titan.GetDefaultClient()
	ctx1, _ := createContext(careProviderId1)
	ctx2, _ := createContext(careProviderId2)
	db = Db{
		AuthorClient:  mongodb.InitDbClient(natClient, "test_db", AuthorCollectionName, false),
		PostClient:    mongodb.InitDbClient(natClient, "test_db", PostCollectionName, false),
		UserClient:    mongodb.InitDbClient(natClient, "test_db", UserCollectionName, false),
		ArticleClient: mongodb.InitDbClient(natClient, "test_db", ArticleCollectionName, true),
		Context1:      ctx1,
		Context2:      ctx2,
	}

	server, cancel := newDBGateWay()

	ch := make(chan interface{}, 1)
	go func() {
		server.Start(ch)
	}()

	select {
	case <-ch:
	case <-time.After(time.Duration(2) * time.Second):
		fmt.Printf("Servers start timed out after %d seconds \n", 2)
		os.Exit(1)
	}

	// before
	mrun := m.Run()
	//
	cancel()
	server.Stop()

	// after
	os.Exit(mrun)
}

func createContext(careProviderId string) (*titan.Context, error) {
	userInfo := titan.UserInfo{
		ExternalUserId:  "",
		UserId:          "",
		CareProviderId:  titan.UUID(careProviderId),
		CareProviderKey: "",
		DeviceId:        "",
		Role:            infra.CARE_PROVIDER_MEMBER,
		Attributes:      map[string]interface{}{},
	}
	ctx := titan.NewContext(context.Background()).WithValue(titan.XUserInfo, &userInfo)

	return ctx, nil
}

func createCareConsumerContext() *titan.Context {
	userInfo := titan.UserInfo{
		ExternalUserId:  "",
		UserId:          "",
		CareProviderId:  "",
		CareProviderKey: "",
		DeviceId:        "",
		Role:            infra.PATIENT,
		Attributes:      map[string]interface{}{},
	}
	return titan.NewContext(context.Background()).WithValue(titan.XUserInfo, &userInfo)
}

func newDBGateWay() (*titan.Server, context.CancelFunc) {

	service, cancel := newDBService()

	router := dbgateway.NewDbGatewayServiceRouter(service)

	server := titan.NewServer(
		viper.GetString("nats.subject"),
		titan.Routes(router.Register),
	)
	return server, cancel
}

func newDBService() (dbgateway.DbGatewayService, context.CancelFunc) {
	dbConfig := mongodb.NewDbConfig()
	testDb := mongodb.NewDatabase(DatabaseName, map[string]bool{
		AuthorCollectionName:  false,
		PostCollectionName:    false,
		UserCollectionName:    false,
		ArticleCollectionName: true,
	})
	dbConfig.Add(testDb)

	auth := authz.NewAuthz(authz.NewDbPolicies())
	auth.AddPolicy(DatabaseName, authz.Policy{
		Collection: AuthorCollectionName,
		Field:      "careProviderId",
		Group:      authz.CARE_PROVIDER,
		Read:       authz.READ_SELF_DATA,
		Write:      authz.WRITE_SELF_DATA,
	})

	auth.AddPolicy(DatabaseName, authz.Policy{
		Collection: PostCollectionName,
		Field:      "careProviderId",
		Group:      authz.CARE_PROVIDER,
		Read:       authz.READ_SELF_DATA,
		Write:      authz.WRITE_SELF_DATA,
	})

	auth.AddPolicy(DatabaseName, authz.Policy{
		Collection: AuthorCollectionName,
		Field:      "authorId",
		Group:      authz.CARE_CONSUMER,
		Read:       authz.READ_ALL_DATA,
		Write:      authz.WRITE_SELF_DATA,
	})

	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	service := service.NewDBGatewayWithAuth(ctx, viper.GetString("mongodb.uri"), dbConfig, &auth)
	return service, cancel
}

func prepareData() (err error) {
	gofakeit.Seed(1001)

	ctxs := []*titan.Context{db.Context1, db.Context2}
	for i := 0; i < len(ctxs); i++ {
		_, _ = db.AuthorClient.Delete(ctxs[i], bson.M{})
		_, _ = db.PostClient.Delete(ctxs[i], bson.M{})
	}

	for i := 0; i < len(ctxs); i++ {
		authors := make([]Author, 5)
		for j := 0; j < 5; j++ {
			authors[j] = Author{
				Id:             fakeUUID(),
				Name:           fmt.Sprintf("Author %d", j),
				Phone:          fmt.Sprintf("phone  %d", j),
				CareProviderId: ctxs[i].UserInfo().CareProviderUUID(),
			}
		}

		var createdAuthors []Author
		err = db.AuthorClient.Create(ctxs[i], authors, &createdAuthors)
		if err != nil {
			return errors.WithMessage(err, "create authors error")
		}

		posts := make([]Post, 5)
		for j := 0; j < 5; j++ {
			posts[j] = Post{
				Id:             fakeUUID(),
				Title:          fmt.Sprintf("post title %d", j),
				Description:    fmt.Sprintf("post description %d", j),
				AuthorId:       authors[j].Id,
				CareProviderId: ctxs[i].UserInfo().CareProviderUUID(),
			}
		}
		var createdPosts []Post
		err = db.PostClient.Create(ctxs[i], posts, &createdPosts)
		if err != nil {
			return errors.WithMessage(err, "create post")
		}
	}
	return nil
}

func fakeUUID() *uuid.UUID {
	id, _ := uuid.Parse(gofakeit.UUID())
	return &id
}

func TestFindOne(t *testing.T) {
	assert := assert.New(t)
	err := prepareData()
	assert.Nil(err)
	var first Author
	err = db.AuthorClient.FindOne(db.Context2, bson.M{}, &first)
	assert.Nil(err)
	assert.Equal(first.CareProviderId.String(), db.Context2.UserInfo().CareProviderId.String())
}

func TestCount(t *testing.T) {
	assert := assert.New(t)
	err := prepareData()
	assert.Nil(err)

	resp, err := db.AuthorClient.Count(db.Context1, bson.M{})
	assert.Nil(err)
	assert.Equal(int64(5), resp.Count)

	careConsumerCtx := createCareConsumerContext()
	resp, err = db.AuthorClient.Count(careConsumerCtx, bson.M{})
	assert.Nil(err)
	assert.Equal(int64(10), resp.Count)
}

func TestFindOneAndReplace(t *testing.T) {
	assert := assert.New(t)
	err := prepareData()
	assert.Nil(err)

	author := Author{
		//Id:             fakeUUID(),
		Name:           "Author replaced",
		Phone:          "phone  replaced",
		CareProviderId: db.Context1.UserInfo().CareProviderUUID(),
	}

	dbs, cancel := newDBService()
	defer cancel()
	filter, _ := bson2.Byte(bson.M{})
	doc, _ := bson2.Byte(author)

	_, err = dbs.FindOneAndReplace(db.Context1, &dbgateway.FindOneAndReplaceRequest{
		DatabaseName:   DatabaseName,
		CollectionName: AuthorCollectionName,
		Filter:         filter,
		Document:       doc,
	})

	assert.Nil(err)

	_, err = dbs.FindOneAndReplace(db.Context2, &dbgateway.FindOneAndReplaceRequest{
		DatabaseName:   DatabaseName,
		CollectionName: AuthorCollectionName,
		Filter:         filter,
		Document:       doc,
	})

	assert.NotNil(err)
}

func TestUpdate(t *testing.T) {
	assert := assert.New(t)
	require := require.New(t)
	err := prepareData()
	require.Nil(err)

	var authors []Author
	err = db.AuthorClient.Find(db.Context1, bson.M{}, &authors)
	require.True(len(authors) > 0)

	author := authors[0]
	author.Name = "updated author"

	var updatedAuthor Author
	err = db.AuthorClient.Update(db.Context1, author, &updatedAuthor)
	require.Nil(err)

	assert.NotNil(updatedAuthor)
	assert.Equal(author.Name, updatedAuthor.Name)
	assert.Equal(author.Id, updatedAuthor.Id)
}

func TestFindOneAndUpdate(t *testing.T) {
	assert := assert.New(t)
	err := prepareData()
	assert.Nil(err)

	update := bson.D{{
		Key: "$set",
		Value: bson.D{
			{Key: "name", Value: "author ctx1 updated"},
		},
	}}

	dbs, cancel := newDBService()
	defer cancel()
	filter, _ := bson2.Byte(bson.M{})
	doc, _ := bson2.Byte(update)

	_, err = dbs.FindOneAndUpdate(db.Context1, &dbgateway.FindOneUpdateRequest{
		DatabaseName:   DatabaseName,
		CollectionName: AuthorCollectionName,
		Filter:         filter,
		Update:         doc,
	})

	assert.Nil(err)

	update2 := bson.D{{
		Key: "$set",
		Value: bson.D{
			{Key: "name", Value: "author ctx2 updated"},
			{Key: "careProviderId", Value: db.Context1.UserInfo().CareProviderUUID()},
		},
	}}
	doc2, _ := bson2.Byte(update2)

	_, err = dbs.FindOneAndUpdate(db.Context2, &dbgateway.FindOneUpdateRequest{
		DatabaseName:   DatabaseName,
		CollectionName: AuthorCollectionName,
		Filter:         filter,
		Update:         doc2,
	})

	assert.NotNil(err)
}

func TestUpdateMany(t *testing.T) {

	assert := assert.New(t)
	err := prepareData()
	assert.Nil(err)

	update := bson.D{{
		Key: "$set",
		Value: bson.D{
			{Key: "name", Value: "author ctx1 updated"},
		},
	}}

	dbs, cancel := newDBService()
	defer cancel()
	filter, _ := bson2.Byte(bson.M{})
	doc, _ := bson2.Byte(update)

	_, err = dbs.UpdateMany(db.Context1, &dbgateway.UpdateManyRequest{
		DatabaseName:   DatabaseName,
		CollectionName: AuthorCollectionName,
		Filter:         filter,
		Update:         doc,
	})

	assert.Nil(err)

	update2 := bson.D{{
		Key: "$set",
		Value: bson.D{
			{Key: "name", Value: "author ctx2 updated"},
			{Key: "careProviderId", Value: db.Context1.UserInfo().CareProviderUUID()},
		},
	}}
	doc2, _ := bson2.Byte(update2)

	_, err = dbs.UpdateMany(db.Context2, &dbgateway.UpdateManyRequest{
		DatabaseName:   DatabaseName,
		CollectionName: AuthorCollectionName,
		Filter:         filter,
		Update:         doc2,
	})

	assert.NotNil(err)

}

func TestDelete(t *testing.T) {
	assert := assert.New(t)
	err := prepareData()
	assert.Nil(err)

	filter, _ := bson2.Byte(bson.M{})

	dbs, cancel := newDBService()
	defer cancel()

	resp, err := dbs.Delete(db.Context1, &dbgateway.DeleteRequest{
		DatabaseName:   DatabaseName,
		CollectionName: AuthorCollectionName,
		Filter:         filter,
	})
	assert.Nil(err)
	assert.Equal(int64(5), resp.DeletedCount)
}

func TestAggregateFilters(t *testing.T) {
	assert := assert.New(t)
	err := prepareData()
	assert.Nil(err)

	filter, _ := bson2.Byte(bson.M{
		"$match": bson.D{{Key: "name", Value: "Author 0"}},
	})

	dbs, cancel := newDBService()
	defer cancel()

	resp, err := dbs.Aggregate(db.Context1, &dbgateway.AggregateRequest{
		DatabaseName:   DatabaseName,
		CollectionName: AuthorCollectionName,
		Filters:        [][]byte{filter},
	})

	assert.Nil(err)
	assert.Equal(1, len(resp.Documents))
}

func TestCollectionWithoutPolicy(t *testing.T) {
	assert := assert.New(t)
	err := prepareLegacyData()
	assert.Nil(err)

	var user User
	err = db.UserClient.FindOne(db.Context1, bson.M{"name": "User 1-1"}, &user)
	assert.Nil(err)
	assert.Equal(db.Context2.UserInfo().CareProviderId.String(), user.CareProviderId.String())

	resp, err := db.ArticleClient.Count(db.Context1, bson.M{})

	assert.Nil(err)

	assert.Equal(int64(5), resp.Count)

}

func prepareLegacyData() error {
	gofakeit.Seed(1001)

	ctxs := []*titan.Context{db.Context1, db.Context2}
	for i := 0; i < len(ctxs); i++ {
		_, _ = db.UserClient.Delete(ctxs[i], bson.M{})
		_, _ = db.ArticleClient.Delete(ctxs[i], bson.M{})
	}

	for i := 0; i < len(ctxs); i++ {
		users := make([]User, 5)
		for j := 0; j < 5; j++ {
			users[j] = User{
				Id:             fakeUUID(),
				Name:           fmt.Sprintf("User %d-%d", i, j),
				Phone:          fmt.Sprintf("phone  %d-%d", i, j),
				CareProviderId: ctxs[i].UserInfo().CareProviderUUID(),
			}
		}

		var createdUsers []User
		err := db.UserClient.Create(ctxs[i], users, &createdUsers)
		if err != nil {
			return errors.WithMessage(err, "create authors error")
		}

		articles := make([]Article, 5)
		for j := 0; j < 5; j++ {
			articles[j] = Article{
				Id:             fakeUUID(),
				Title:          fmt.Sprintf("article title %d", j),
				Description:    fmt.Sprintf("article description %d", j),
				UserId:         users[j].Id,
				CareProviderId: ctxs[i].UserInfo().CareProviderUUID(),
			}
		}
		var createdArticles []Article
		err = db.ArticleClient.Create(ctxs[i], articles, &createdArticles)
		if err != nil {
			return errors.WithMessage(err, "create article")
		}
	}
	return nil
}
