package authz_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/silenteer-oss/hestia/db_gateway_service/pkg/authz"
)

func TestColPolicies_GivenColPolicies_MustAddSuccessfully(t *testing.T) {
	colPolicies := authz.NewColPolicies()

	collectionName := "test_col1"

	colPolicies.Add(authz.Policy{
		Collection: collectionName,
		Field:      "",
		Group:      "",
		Read:       "",
		Write:      "",
	})

	colPolicies.Add(authz.Policy{
		Collection: collectionName,
		Field:      "",
		Group:      "",
		Read:       "",
		Write:      "",
	})

	assert.Equal(t, 1, len(colPolicies))
	assert.Equal(t, 2, len(colPolicies[collectionName]))
	assert.Equal(t, 0, len(colPolicies["not exist name"]))
}

func TestDbPolicies_GivenDbPolicies_MustAddSuccessfully(t *testing.T) {
	dbPolicies := authz.NewDbPolicies()
	assert.Equal(t, 0, len(dbPolicies))

	collectionName := "test_col1"
	dbName1 := "test1"
	dbName2 := "test2"

	dbPolicies.Add(dbName1, authz.Policy{
		Collection: collectionName,
		Field:      "",
		Group:      "",
		Read:       "",
		Write:      "",
	})

	dbPolicies.Add(dbName1, authz.Policy{
		Collection: collectionName,
		Field:      "",
		Group:      "",
		Read:       "",
		Write:      "",
	})

	dbPolicies.Add(dbName2, authz.Policy{
		Collection: collectionName,
		Field:      "",
		Group:      "",
		Read:       "",
		Write:      "",
	})

	assert.Equal(t, 2, len(dbPolicies))
	assert.Equal(t, 1, len(dbPolicies[dbName1]))
	assert.Equal(t, 1, len(dbPolicies[dbName2]))
	assert.Equal(t, 2, len(dbPolicies[dbName1][collectionName]))
	assert.Equal(t, 1, len(dbPolicies[dbName2][collectionName]))
}
