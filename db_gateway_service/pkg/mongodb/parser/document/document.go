package document

import (
	"gitlab.com/silenteer-oss/hestia/db_gateway_service/pkg/mongodb/parser/bson"
	"reflect"
)

// Bytes convert interface into BDocument Array [][]Byte
// The data interface can be struct or array of interface{} or bson type.
// If the data is not slice, the return will be an array with 1 item
func Bytes(data interface{}) ([][]byte, error) {
	isArray := IsArray(data)

	if isArray {
		return bson.ToBytesArrays(data)
	}

	document, err := bson.Byte(data)
	if err != nil {
		return nil, err
	}
	var documents [][]byte
	documents = append(documents, document)

	return documents, err
}

// Entity convert document byte into BDocument interface
func Entity(document []byte, receiver interface{}) error {
	return bson.ToEntity(document, receiver)
}

// Entities convert [][]byte into interface model
func Entities(documents [][]byte, receiver interface{}) error {
	sliceType := reflect.TypeOf(receiver).Elem()
	array := reflect.MakeSlice(sliceType, 0, 0)

	for i := range documents {
		document := documents[i]

		refType := reflect.New(sliceType.Elem())

		err := Entity(document, refType.Interface())

		if err != nil {
			return err
		}

		array = reflect.Append(array, refType.Elem())
	}

	reflect.ValueOf(receiver).Elem().Set(array)

	return nil
}

// IsArray check of the data isa type of Array or Slice
func IsArray(data interface{}) bool {
	rv := reflect.ValueOf(data)
	return rv.Kind() == reflect.Array || rv.Kind() == reflect.Slice
}
