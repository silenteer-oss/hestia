package service

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"go.mongodb.org/mongo-driver/bson"
)

func TestService_CreateIndexModel(t *testing.T) {
	t.Run("test with simple index", func(t *testing.T) {
		var value interface{} = bson.M{"v": 2, "key": bson.M{"_id": 1}, "name": "_id", "ns": "tutum_profile_db.employeeprofile.none.delete.multitenant.index.template", "unique": true}
		data, err := bson.Marshal(value)
		assert.Equal(t, err, nil)
		version := int32(2)
		name := "\"_id\""
		keys, _ := bson.Marshal(bson.M{"_id": 1})
		var keysRaw bson.Raw = keys
		actual, err := createIndexModel(data)
		assert.Equal(t, err, nil)
		assert.Equal(t, version, *actual.Options.Version)
		assert.Equal(t, name, *actual.Options.Name)
		assert.Equal(t, keysRaw, actual.Keys)
		assert.Equal(t, true, *actual.Options.Unique)
	})

}
