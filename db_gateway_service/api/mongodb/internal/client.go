package internal

import (
	"fmt"
	"sync"

	"gitlab.com/silenteer-oss/hestia/db_gateway_service/api/mongodb/constant"

	"emperror.dev/errors"
	"github.com/google/uuid"
	"gitlab.com/silenteer-oss/hestia/db_gateway_service/api/dbgateway"
	clientModel "gitlab.com/silenteer-oss/hestia/db_gateway_service/api/mongodb/model"
	"gitlab.com/silenteer-oss/hestia/db_gateway_service/pkg/mongodb/parser/bson"
	"gitlab.com/silenteer-oss/hestia/db_gateway_service/pkg/mongodb/parser/document"
	"gitlab.com/silenteer-oss/titan"
	bson2 "go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type DbClient struct {
	NatClient      *titan.Client //unused
	DBName         string
	CollectionName string
	Tenant         bool
	proxy          dbgateway.DbGatewayService
	syncOnce       sync.Once
}

func (client *DbClient) getProxy() dbgateway.DbGatewayService {
	if client.proxy != nil {
		return client.proxy
	}
	client.syncOnce.Do(func() {
		client.proxy = dbgateway.NewDbGatewayServiceClient()
	})
	return client.proxy
}

// TODO: have not finished implement transactionId for transaction.make test
func (client *DbClient) OpenTransaction(ctx *titan.Context, transactionId uuid.UUID) error {
	requestModel := dbgateway.Transaction{
		Header:        initHeader(ctx),
		TransactionId: &transactionId,
	}
	return client.getProxy().OpenTransaction(ctx, &requestModel)
}

func (client *DbClient) CommitTransaction(ctx *titan.Context, transactionId uuid.UUID) error {
	requestModel := dbgateway.Transaction{
		Header:        initHeader(ctx),
		TransactionId: &transactionId,
	}
	return client.getProxy().CommitTransaction(ctx, &requestModel)
}

func (client *DbClient) AbortTransaction(ctx *titan.Context, transactionId uuid.UUID) error {
	requestModel := dbgateway.Transaction{
		Header:        initHeader(ctx),
		TransactionId: &transactionId,
	}
	return client.getProxy().AbortTransaction(ctx, &requestModel)
}

func (client *DbClient) CloseTransaction(ctx *titan.Context, transactionId uuid.UUID) error {
	requestModel := dbgateway.Transaction{
		Header:        initHeader(ctx),
		TransactionId: &transactionId,
	}
	return client.getProxy().CloseTransaction(ctx, &requestModel)
}

func (client *DbClient) Create(ctx *titan.Context, data interface{}, receiver interface{}) error {
	isArray := document.IsArray(data)
	documents, err := document.Bytes(data)

	if err != nil {
		return errors.WithMessage(err, "create.convert.to.bytes.error:")
	}
	collectionName, err := client.GetCollectionName(ctx)
	if err != nil {
		return errors.WithMessage(err, "create GetCollectionName error:")
	}

	requestDto := dbgateway.CreateRequest{
		DatabaseName:   client.DBName,
		CollectionName: collectionName,
		TransactionId:  nil,
		Header:         initHeader(ctx),
		Documents:      documents,
	}

	result, err := client.getProxy().Create(ctx, &requestDto)
	if err != nil {
		return errors.WithMessage(err, "create.nat.send.and.receive.error:")
	}

	if isArray && receiver != nil {
		err = document.Entities(result.Documents, receiver)

		if err != nil {
			return errors.WithMessage(err, "create.convert.to.entities.error:")
		}

		return nil
	}
	if len(result.Documents) == 0 {
		return fmt.Errorf("create.no.response.data")
	}
	if receiver != nil {
		if err = document.Entity(result.Documents[0], receiver); err != nil {
			return errors.WithMessage(err, "create.convert.to.entity.error:")
		}
	}

	return nil
}

// UpdateManyRequest find one and replace with option return after.
// Using _id to filter
func (client *DbClient) Update(ctx *titan.Context, data interface{}, receiver interface{}) error {
	isArray := document.IsArray(data)
	documents, err := document.Bytes(data)

	if err != nil {
		return errors.WithMessage(err, "create.convert.to.bytes.error:")
	}

	collectionName, err := client.GetCollectionName(ctx)
	if err != nil {
		return errors.WithMessage(err, "update GetCollectionName error")
	}

	requestDto := dbgateway.UpdateRequest{
		DatabaseName:   client.DBName,
		CollectionName: collectionName,
		TransactionId:  nil,
		Documents:      documents,
		Header:         initHeader(ctx),
	}

	result, err := client.getProxy().Update(ctx, &requestDto)

	if err != nil {
		return errors.WithMessage(err, "create.nat.send.and.receive.error:")
	}

	if isArray && receiver != nil {
		err = document.Entities(result.Documents, receiver)

		if err != nil {
			return errors.WithMessage(err, "create.convert.to.entities.error:")
		}

		return nil
	}

	if len(result.Documents) == 0 {
		return fmt.Errorf("create.no.response.data")
	}
	if receiver != nil {
		if err = document.Entity(result.Documents[0], receiver); err != nil {
			return errors.WithMessage(err, "create.convert.to.entity.error:")
		}
	}

	return err
}

// PartialUpdate exec query {$set: partialData}
// with omitempty: will ignore nil pointer and zero value
// without omitempty: the nil pointer and zero value are still be used in $set query.
func (client *DbClient) PartialUpdate(ctx *titan.Context, filter interface{}, partialData interface{},
	receiver interface{}, ops ...*options.UpdateOptions) (*dbgateway.UpdateManyResponse, error) {
	marshedBytes, err := bson.Byte(partialData)
	if err != nil {
		return nil, errors.WithMessage(err, "PartialUpdate to bson.Byte error")
	}
	bsonDocument := bson2.D{}
	err = bson.ToEntity(marshedBytes, &bsonDocument)
	bsonSetDocument := bson2.D{
		{
			"$set",
			bsonDocument,
		},
	}
	returnModified := true
	responseUpdate, err := client.UpdateMany(ctx, filter, bsonSetDocument, receiver, &returnModified, ops...)
	if err != nil {
		return responseUpdate, errors.WithMessage(err, "PartialUpdate UpdateMany error")
	}
	return responseUpdate, nil
}

func (client *DbClient) FindOneAndUpdate(
	ctx *titan.Context,
	filter interface{},
	update interface{},
	receiver interface{},
	opts ...*options.FindOneAndUpdateOptions,
) error {
	collectionName, err := client.GetCollectionName(ctx)
	if err != nil {
		return errors.WithMessage(err, "FindOneAndUpdate GetCollectionName error")
	}
	return client.findOneAndUpdate(ctx, collectionName, filter, update, receiver, opts...)

}

func (client *DbClient) findOneAndUpdate(
	ctx *titan.Context,
	collectionName string,
	filter interface{},
	update interface{},
	receiver interface{},
	opts ...*options.FindOneAndUpdateOptions,
) error {
	fo := options.MergeFindOneAndUpdateOptions(opts...)

	f, err := bson.Byte(filter)
	if err != nil {
		return err
	}
	fupdate, err := bson.Byte(update)
	if err != nil {
		return err
	}
	fob, err := bson.Byte(fo)
	if err != nil {
		return err
	}
	requestModel := dbgateway.FindOneUpdateRequest{
		DatabaseName:   client.DBName,
		CollectionName: collectionName,
		TransactionId:  nil,
		Header:         initHeader(ctx),
		Filter:         f,
		Update:         fupdate,
		Option:         &fob,
	}

	result, err := client.getProxy().FindOneAndUpdate(ctx, &requestModel)

	if err != nil {
		return errors.WithMessage(err, "find.one.and.update.nat.send.and.receive.error:")
	}

	if result.Document == nil {
		return nil
	}
	if receiver != nil {
		if err = document.Entity(result.Document, receiver); err != nil {
			return errors.WithMessage(err, "find.one.and.update.convert.to.entity.error:")
		}
	}
	return err
}

func (client *DbClient) FindOneAndReplace(
	ctx *titan.Context,
	filter interface{},
	replacement interface{},
	receiver interface{},
	opts ...*options.FindOneAndReplaceOptions) error {
	f, fError := bson.Byte(filter)
	byteDoc, uError := bson.Byte(replacement)

	if fError != nil {
		return errors.WithMessage(fError, "find.one.and.replace.convert.filter.to.bytes.error:")
	}

	if uError != nil {
		return errors.WithMessage(uError, "find.one.and.replace.convert.replacement.to.bytes.error:")
	}

	collectionName, err := client.GetCollectionName(ctx)
	if err != nil {
		return errors.WithMessage(err, "FindOneAndReplace GetCollectionName error")
	}

	fo := options.MergeFindOneAndReplaceOptions(opts...)

	requestDto := dbgateway.FindOneAndReplaceRequest{
		DatabaseName:   client.DBName,
		CollectionName: collectionName,
		TransactionId:  nil,
		Header:         initHeader(ctx),
		Document:       byteDoc,
		Filter:         f,
	}
	if fo.ReturnDocument != nil {
		r := int32(*fo.ReturnDocument)
		requestDto.ReturnNewDocument = &r
	}
	if fo.Upsert != nil && *fo.Upsert {
		requestDto.Upsert = fo.Upsert
	}

	result, err := client.getProxy().FindOneAndReplace(ctx, &requestDto)

	if err != nil {
		return errors.WithMessage(err, "find.one.and.replace.nat.send.and.receive.error:")
	}

	if result.Document == nil {
		return nil
	}

	if receiver != nil {
		if err = document.Entity(result.Document, receiver); err != nil {
			return errors.WithMessage(err, "find.one.and.replace.convert.to.entity.error:")
		}
	}

	return err
}

func (client *DbClient) Find(
	ctx *titan.Context,
	filter interface{},
	receiver interface{},
	opts ...*options.FindOptions) error {
	if receiver == nil {
		return errors.New("receiver is nil")
	}
	f, fError := bson.Byte(filter)

	if fError != nil {
		return errors.WithMessage(fError, "find.convert.filter.to.bytes.error:")
	}

	collectionName, err := client.GetCollectionName(ctx)
	if err != nil {
		return errors.WithMessage(err, "Find GetCollectionName error")
	}

	fo := options.MergeFindOptions(opts...)

	var prj []byte
	var pError error
	if fo.Projection != nil {
		prj, pError = bson.Byte(fo.Projection)

		if pError != nil {
			return errors.WithMessage(pError, "find.convert.projection.to.bytes.error:")
		}
	}

	var srt []byte
	var sError error
	if fo.Sort != nil {
		srt, sError = bson.Byte(fo.Sort)

		if sError != nil {
			return errors.WithMessage(sError, "find.convert.sort.to.bytes.error:")
		}
	}

	requestDto := dbgateway.ReadRequest{
		DatabaseName:   client.DBName,
		CollectionName: collectionName,
		TransactionId:  nil,
		Header:         initHeader(ctx),
		Filter:         f,
		Projection:     &prj,
		Sort:           &srt,
		Skip:           fo.Skip,
		Limit:          fo.Limit,
	}

	result, err := client.getProxy().Read(ctx, &requestDto)

	if err != nil {
		return errors.WithMessage(err, "find.nat.send.and.receive.error:")
	}

	if err = document.Entities(result.Documents, receiver); err != nil {
		return errors.WithMessage(err, "find.convert.to.entities.error:")
	}

	return err
}

func (client *DbClient) FindOne(
	ctx *titan.Context,
	filter interface{},
	receiver interface{},
	opts ...*options.FindOneOptions) error {
	if receiver == nil {
		return errors.New("receiver is nil")
	}
	f, fError := bson.Byte(filter)
	if fError != nil {
		return errors.WithMessage(fError, "find.convert.filter.to.bytes.error:")
	}

	collectionName, err := client.GetCollectionName(ctx)
	if err != nil {
		return errors.WithMessage(err, "FindOne GetCollectionName error")
	}

	fo := options.MergeFindOneOptions(opts...)

	var prj []byte
	var pError error
	if fo.Projection != nil {
		prj, pError = bson.Byte(fo.Projection)

		if pError != nil {
			return errors.WithMessage(pError, "find.convert.projection.to.bytes.error:")
		}
	}

	var srt []byte
	var sError error
	if fo.Sort != nil {
		srt, sError = bson.Byte(fo.Sort)

		if sError != nil {
			return errors.WithMessage(sError, "find.convert.sort.to.bytes.error:")
		}
	}

	requestDto := dbgateway.FindOneRequest{
		DatabaseName:   client.DBName,
		CollectionName: collectionName,
		TransactionId:  nil,
		Header:         initHeader(ctx),
		Filter:         f,
		Projection:     &prj,
		Sort:           &srt,
		Skip:           fo.Skip,
	}

	result, err := client.getProxy().FindOne(ctx, &requestDto)

	if err != nil {
		return errors.WithMessage(err, "find.nat.send.and.receive.error:")
	}

	if err = bson.ToEntity(result.Document, receiver); err != nil {
		return errors.WithMessage(err, "find.convert.to.entities.error:")
	}

	return err
}

func (client *DbClient) Aggregate(
	ctx *titan.Context,
	filters interface{},
	receiver interface{},
	opts ...*options.AggregateOptions) error {
	if receiver == nil {
		return errors.New("receiver is nil")
	}
	stages, err := document.Bytes(filters)
	if err != nil {
		return errors.WithMessage(err, "aggregate.convert.filter.to.bytes.error:")
	}

	fo := options.MergeAggregateOptions(opts...)

	collectionName, err := client.GetCollectionName(ctx)
	if err != nil {
		return errors.WithMessage(err, "Aggregate GetCollectionName error")
	}

	requestDto := dbgateway.AggregateRequest{
		DatabaseName:   client.DBName,
		CollectionName: collectionName,
		TransactionId:  nil,
		Header:         initHeader(ctx),
		Filters:        stages,
	}

	if fo.BatchSize != nil {
		requestDto.BatchSize = *fo.BatchSize
	}
	if fo.BatchSize != nil {
		requestDto.AllowDiskUse = *fo.AllowDiskUse
	}

	result, err := client.getProxy().Aggregate(ctx, &requestDto)

	if err != nil {
		return errors.WithMessage(err, "aggregate.nat.send.and.receive.error:")
	}

	if err = document.Entities(result.Documents, receiver); err != nil {
		return errors.WithMessage(err, "aggregate.convert.to.entities.error:")
	}

	return err
}

func (client *DbClient) Delete(
	ctx *titan.Context,
	filter interface{},
) (*dbgateway.DeleteResponse, error) {
	f, fError := bson.Byte(filter)

	if fError != nil {
		return nil, errors.WithMessage(fError, "delete.convert.filter.to.bytes.error:")
	}

	collectionName, err := client.GetCollectionName(ctx)
	if err != nil {
		return nil, errors.WithMessage(err, "Delete GetCollectionName error")
	}

	requestDto := dbgateway.DeleteRequest{
		DatabaseName:   client.DBName,
		CollectionName: collectionName,
		TransactionId:  nil,
		Header:         initHeader(ctx),
		Filter:         f,
	}

	return client.getProxy().Delete(ctx, &requestDto)
}

func (client *DbClient) Count(
	ctx *titan.Context,
	filter interface{}) (*dbgateway.CountResponse, error) {
	f, fError := bson.Byte(filter)

	if fError != nil {
		return nil, errors.WithMessage(fError, "count.convert.filter.to.bytes.error:")
	}

	collectionName, err := client.GetCollectionName(ctx)
	if err != nil {
		return nil, errors.WithMessage(err, "Count GetCollectionName error")
	}

	requestDto := dbgateway.CountRequest{
		DatabaseName:   client.DBName,
		CollectionName: collectionName,
		TransactionId:  nil,
		Header:         initHeader(ctx),
		Filter:         f,
	}

	return client.getProxy().Count(ctx, &requestDto)
}

// UpdateMany
// returnModifiedDocument = nil or false, the function will not return and parse result the updated documents.
// receiver can be nil. When receiver is nil, the function will not parse result into receiver.
// Upsert = true, should set _id = <VALUE> in query since the mongoDB can't auto generate _id UUID.
func (client *DbClient) UpdateMany(ctx *titan.Context, filter interface{}, update interface{},
	receiver interface{}, returnModifiedDocument *bool, ops ...*options.UpdateOptions) (*dbgateway.UpdateManyResponse, error) {

	f, fError := bson.Byte(filter)
	u, uError := bson.Byte(update)
	opts := options.MergeUpdateOptions(ops...)

	if fError != nil {
		return nil, errors.WithMessage(fError, "update.many.convert.filter.to.bytes.error:")
	}

	if uError != nil {
		return nil, errors.WithMessage(uError, "update.many.convert.update.content.to.bytes.error:")
	}

	collectionName, err := client.GetCollectionName(ctx)
	if err != nil {
		return nil, errors.WithMessage(err, "UpdateMany GetCollectionName error")
	}

	requestDto := dbgateway.UpdateManyRequest{
		DatabaseName:   client.DBName,
		CollectionName: collectionName,
		TransactionId:  nil,
		Header:         initHeader(ctx),
		Filter:         f,
		Update:         u,
		Upsert:         opts.Upsert,
		ReturnModified: returnModifiedDocument,
	}

	result, err := client.getProxy().UpdateMany(ctx, &requestDto)

	if err != nil {
		return nil, errors.WithMessage(err, "update.many.nat.send.and.receive.error:")
	}
	if receiver != nil && returnModifiedDocument != nil && *returnModifiedDocument {
		if err = document.Entities(result.Documents, receiver); err != nil {
			return nil, errors.WithMessage(err, "UpdateMany To Entities error")
		}
	}

	return result, err
}

// GetConsequenceNumber auto get the nex consequence number based on the current collection of ctx.
// return -1, err if there is a error
func (client *DbClient) GetConsequenceNumber(ctx *titan.Context) (int64, error) {
	collectionName, err := client.GetCollectionName(ctx)
	if err != nil {
		return -1, errors.WithMessage(err, "GetConsequenceNumber")
	}
	filter := bson2.D{
		{
			Key:   "collectionName",
			Value: collectionName,
		},
	}
	update := bson2.M{
		"$inc": bson2.M{"collectionSequenceNumber": 1},
	}
	returnDoc := options.After
	upsert := true
	opts := options.FindOneAndUpdateOptions{
		ArrayFilters:             nil,
		BypassDocumentValidation: nil,
		Collation:                nil,
		MaxTime:                  nil,
		Projection:               nil,
		ReturnDocument:           &returnDoc,
		Sort:                     nil,
		Upsert:                   &upsert,
	}
	var sequenceNumber clientModel.SequenceNumber

	sequenceCollectionName, err := client.getSequenceCollectionName(ctx)
	if err != nil {
		return -1, errors.WithMessage(err, "getSequenceCollectionName error")
	}

	err = client.findOneAndUpdate(ctx, sequenceCollectionName, filter, update, &sequenceNumber, &opts)

	if err != nil {
		return -1, errors.WithMessage(err, "GetConsequenceNumber.client.findOneAndUpdate")
	}
	return sequenceNumber.CollectionSequenceNumber, nil
}

func (client *DbClient) Collections(
	ctx *titan.Context,
	collectionName string) (*dbgateway.CollectionResponse, error) {

	requestDto := dbgateway.CollectionRequest{
		DatabaseName:   client.DBName,
		TransactionId:  nil,
		Header:         initHeader(ctx),
		CollectionName: collectionName,
	}

	return client.getProxy().Collections(ctx, &requestDto)
}

func (client *DbClient) getSequenceCollectionName(ctx *titan.Context) (string, error) {
	if ctx == nil || ctx.UserInfo() == nil || ctx.UserInfo().CareProviderId == "" {
		return "", errors.New("user information or care provider id is empty")
	}
	return fmt.Sprintf("%s.%s", string(constant.SequenceCollection), ctx.UserInfo().CareProviderId), nil
}

func (client *DbClient) GetCollectionName(ctx *titan.Context) (string, error) {
	if client.Tenant {
		if ctx == nil || ctx.UserInfo() == nil || ctx.UserInfo().CareProviderId == "" {
			return "", errors.New("user information or care provider id is empty")
		}
		return fmt.Sprintf("%s.%s", client.CollectionName, ctx.UserInfo().CareProviderId), nil
	}
	return client.CollectionName, nil
}

func (client *DbClient) CreateMultiTenantCollections(
	ctx *titan.Context,
	multiTenantId uuid.UUID) error {

	requestDto := dbgateway.MultiTenantCollectionCreateRequest{
		MultiTenantId: &multiTenantId,
	}

	return client.getProxy().CreateMultiTenantCollections(ctx, &requestDto)
}

func initHeader(ctx *titan.Context) *dbgateway.RequestHeader {
	header := &dbgateway.RequestHeader{
		UserId:         "",
		TraceId:        ctx.RequestId(),
		CareProviderId: "",
		Auditable:      true,
	}

	if ctx != nil && ctx.UserInfo() != nil {
		header.UserId = ctx.UserInfo().UserId.String()
		header.CareProviderId = ctx.UserInfo().CareProviderId.String()
	}
	return header
}
