package mongodb

import (
	"github.com/google/uuid"
	"gitlab.com/silenteer-oss/hestia/db_gateway_service/api/dbgateway"
	natdbclient "gitlab.com/silenteer-oss/hestia/db_gateway_service/api/mongodb/internal"
	"gitlab.com/silenteer-oss/titan"
	"go.mongodb.org/mongo-driver/mongo/options"
)

func InitDbClient(natClient *titan.Client, dbName string,
	collectionName string, isTenant bool) IDBClient {
	return &natdbclient.DbClient{
		NatClient:      natClient,
		DBName:         dbName,
		CollectionName: collectionName,
		Tenant:         isTenant,
	}
}

type IDBClient interface {
	OpenTransaction(ctx *titan.Context, transactionId uuid.UUID) error
	CommitTransaction(ctx *titan.Context, transactionId uuid.UUID) error
	AbortTransaction(ctx *titan.Context, transactionId uuid.UUID) error
	CloseTransaction(ctx *titan.Context, transactionId uuid.UUID) error
	Create(ctx *titan.Context, data interface{}, receiver interface{}) error
	Update(ctx *titan.Context, data interface{}, receiver interface{}) error
	PartialUpdate(ctx *titan.Context, filter interface{}, partialData interface{}, receiver interface{}, ops ...*options.UpdateOptions) (*dbgateway.UpdateManyResponse, error)
	FindOneAndUpdate(ctx *titan.Context, filter interface{}, update interface{}, receiver interface{}, opts ...*options.FindOneAndUpdateOptions) error
	FindOneAndReplace(ctx *titan.Context, filter interface{}, replacement interface{}, receiver interface{}, opts ...*options.FindOneAndReplaceOptions) error
	Find(ctx *titan.Context, filter interface{}, receiver interface{}, opts ...*options.FindOptions) error
	FindOne(ctx *titan.Context, filter interface{}, receiver interface{}, opts ...*options.FindOneOptions) error
	Aggregate(ctx *titan.Context, filters interface{}, receiver interface{}, opts ...*options.AggregateOptions) error
	Delete(ctx *titan.Context, filter interface{}) (*dbgateway.DeleteResponse, error)
	Count(ctx *titan.Context, filter interface{}) (*dbgateway.CountResponse, error)
	UpdateMany(ctx *titan.Context, filter interface{}, update interface{}, receiver interface{}, returnModifiedDocument *bool, ops ...*options.UpdateOptions) (*dbgateway.UpdateManyResponse, error)
	GetConsequenceNumber(ctx *titan.Context) (int64, error)
	Collections(ctx *titan.Context, collectionName string) (*dbgateway.CollectionResponse, error)
	GetCollectionName(ctx *titan.Context) (string, error)
	CreateMultiTenantCollections(ctx *titan.Context, multiTenantId uuid.UUID) error
}
