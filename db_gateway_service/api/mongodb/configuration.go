package mongodb

// database:collection:IsTenant
type Collection struct {
	Name     string
	IsTenant bool
}

type Database struct {
	Name        string
	Collections map[string]*Collection
}

func (d *Database) Set(col *Collection) {
	d.Collections[col.Name] = col
}

func (d *Database) Get(name string) *Collection {
	if val, ok := d.Collections[name]; ok {
		return val
	}
	return nil
}

func NewDatabase(name string, mapOfCollectionNameAndIsTenant map[string]bool) *Database {
	db := &Database{
		Name:        name,
		Collections: make(map[string]*Collection),
	}
	db.AddCollections(mapOfCollectionNameAndIsTenant)
	return db
}

func (d *Database) AddCollections(mapOfCollectionNameAndIsTenant map[string]bool) {
	for name, isTenant := range mapOfCollectionNameAndIsTenant {
		d.Collections[name] = &Collection{
			Name:     name,
			IsTenant: isTenant,
		}
	}
}

type DbConfig struct {
	Databases map[string]*Database
}

func (c *DbConfig) Add(db *Database) {
	if c.Databases == nil {
		c.Databases = make(map[string]*Database)
	}
	c.Databases[db.Name] = db
}

func (c *DbConfig) Get(name string) *Database {
	if val, ok := c.Databases[name]; ok {
		return val
	}
	return nil
}

func (c *DbConfig) GetCollection(dbName string, collectionName string) *Collection {

	db := c.Get(dbName)
	if db == nil {
		return nil
	}
	return db.Get(collectionName)
}

func NewDbConfig(dbs ...*Database) *DbConfig {
	config := &DbConfig{
		Databases: make(map[string]*Database),
	}

	for _, db := range dbs {
		config.Add(db)
	}

	return config
}
