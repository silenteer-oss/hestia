SHELL 	 := /bin/bash

codegen:
	@command -v protoc >/dev/null 2>&1 || { echo >&2 "Error: protoc is not installed.  Aborting."; exit 1; }
	@command -v tsfmt >/dev/null 2>&1 || { echo >&2 "Error: tsfmt is not installed.  Aborting."; exit 1; }
	@go install gitlab.com/silenteer-oss/goff/protoc-gen-goff
	@set -e;\
	for SOURCE in ./proto/*.proto; do \
		if [[ "$$file" != "" ]] && [[  "$$file" != "$$(basename $$SOURCE)" ]]; then \
			continue; \
		fi; \
		echo Generating code from $$SOURCE; \
		protoc  -I `go list -m -f "{{.Dir}}" gitlab.com/silenteer-oss/goff` -I ./proto  --goff_out ./  $$SOURCE; \
		PACKAGE=$$(sed -n -e '/^option go_package/p' $$SOURCE | cut -d = -f 2 | cut -d \" -f 2); \
		TARGET_DIR=$$(echo $$PACKAGE | sed -e "s/^gitlab.com\/silenteer-oss\/hestia/./"); \
		mkdir -p $$TARGET_DIR; \
		cp -rf ./$$PACKAGE/*.go $$TARGET_DIR/; \
		cp -rf ./$$PACKAGE/*.java ./java; \
	done;
	rm -rf ./gitlab.com;

test:
	go test ./...

